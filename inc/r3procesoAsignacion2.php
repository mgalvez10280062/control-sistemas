<?php
class Asignacion{
	//RESPALDO
    function cancelarNoDocumentacion($db_pdo){
		
        $qrySinDoc = "SELECT s.kcvesolicitante as kcve, s.odocumentacionentregada_s as doc
		FROM r3solicitantetutor_td_2 s
		WHERE (s.odocumentacionentregada_s is null or s.odocumentacionentregada_s = 'N')
		AND s.obeneficiado_s is null
		AND s.ocancelado_s is null
		AND (s.oposiblehermano_s is null or s.oposiblehermano_s <> 'R')"; //rojos pendientes

        $rslSinDoc = $db_pdo->query($qrySinDoc) or die("ERROR Query SInDoc... ".$qrySinDoc);

        $cancelados = 0;
		
        while($rowSinDoc = $rslSinDoc->fetch(PDO::FETCH_ASSOC)){
 
			$upSinDoc = "UPDATE r3solicitantetutor_td_2 
			SET ocancelado_s = 'S', ";
            if($rowSinDoc["DOC"] == NULL){
                $upSinDoc = $upSinDoc . " omotivocancelacion_s = 4 ";
            }else{
                $upSinDoc = $upSinDoc . " omotivocancelacion_s = 11 ";
			}	
            $upSinDoc = $upSinDoc . " WHERE kcvesolicitante = ".$rowSinDoc["KCVE"]." 
			AND oejercicio='2011'";

            $cancelados = $cancelados + $db_pdo->exec($upSinDoc) or die("UPDATE Sindoc Error... ". $upSinDoc);
			
        }
        return $cancelados;

		
    }
	
	
	  
    function cancelarHermanos($db_pdo){
		$sql = "SELECT s.kcvesolicitante as kcve,  s.onombre_s, s.oapellidopaterno_s, s.oapellidomaterno_s,
		s.rcvecctactual_s, rcveturno_s, s.onivel_s, s.obeneficiado_s,
		CASE WHEN s.onivel_s = 'PJN' THEN 0
		WHEN s.onivel_s = 'PPR' THEN 1
		WHEN s.onivel_s = 'PES' THEN 2
		WHEN s.onivel_s = 'PST' THEN 3
		END nivel, s.ogrado_s, s.opromedio_s, s.ofoliobeca_s,s.rtipotutor_t, 
		replace(replace(s.onombre_t, \". \", \"\"), \" \", \"\") as TNOM,
		replace(replace(s.oapellidopaterno_t, \". \", \"\"), \" \", \"\") as TAP,
		replace(replace(s.oapellidomaterno_t, \". \", \"\"), \" \", \"\") as TAM
		FROM r3solicitantetutor_td_2 s
				
		WHERE replace(replace(concat(s.oapellidopaterno_s, concat(s.oapellidomaterno_s, concat(s.oapellidopaterno_t, concat(s.oapellidomaterno_t, s.onombre_t)))), \". \", \"\"), \" \", \"\")
		in
		(
			SELECT apt FROM
			(
				SELECT replace(replace(concat(s1.oapellidopaterno_s, concat(s1.oapellidomaterno_s, concat(s1.oapellidopaterno_t, concat(s1.oapellidomaterno_t, s1.onombre_t)))), \". \", \"\"), \" \", \"\") apt
				FROM r3solicitantetutor_td_2 s1
				WHERE s1.odocumentacionentregada_s is not null
				and s1.obeneficiado_s is null
				and s1.ocancelado_s is null
				and (s1.oposiblehermano_s is null OR s1.oposiblehermano_s <> 'R')
			)
			group by apt
			having count(apt) > 1
		)
		
		
		AND s.odocumentacionentregada_s is not null
		AND s.obeneficiado_s is null
		AND s.ocancelado_s is null
		AND (s.oposiblehermano_s is null OR s.oposiblehermano_s <>'R')
		ORDER BY s.oapellidopaterno_s, s.oapellidomaterno_s, s.onivel_s DESC, s.ogrado_s DESC";
	
	
		$rslt = $db_pdo->query($sql) or die($sql);
		$aps = "";
		$cancelados = 0;
   		while($row = $rslt->fetch(PDO::FETCH_ASSOC)){
							
			$apss = $row["TAP"] . $row["TAM"] . $row["TNOM"];
	
			if($aps != $apss){
				$aps = $row["TAP"] . $row["TAM"] . $row["TNOM"];
			}else{
				//cancelado por hermano(3)
				$qrycancel = "UPDATE r3solicitantetutor_td_2 SET
				ocancelado_s = 'S',
				omotivocancelacion_s = 3
				WHERE kcvesolicitante = ".$row["KCVE"]."
				ANd oejercicio='2011'";
				//echo $qrycancel;
				$cancelados = $cancelados + $db_pdo->exec($qrycancel) or die($qrycancel);
				//$cancelados = $cancelados + 1;
			}
				
		
		}
            
		return $cancelados;
    }
	
	
	
	
	
	
	
	
	
    function marcarPosiblesHermanos($db_pdo){
			
		//concatenamos apellidopaterno y materno  APT
		//obtenemos cuales se repiten y los ordenamos por APT
		/*$sql = "SELECT apt FROM
		(
			SELECT concat(s1.oapellidopaterno, s1.oapellidomaterno) apt
			FROM r3solicitante s1
			WHERE odocumentacionentregada = 'S'
			AND obeneficiado is null
			AND ocancelado is null
			AND (oposiblehermano is null OR oposiblehermano <> 'R')
		)
		group by apt
		having count(apt) > 1";*/
			  
			
	
        $qry = "SELECT s.kcvesolicitante as kcve, s.onombre_s, s.oapellidopaterno_s,
		s.oapellidomaterno_s, s.osexo_s, s.ocurp_s, s.ofechadenacimiento_s,
		s.rcveestadooriginario_s, s.odomicilio_s, s.olocalidad_s,
		s.rcvemunicipio_s, s.omunicipioaledano_s, s.rcveestadooriginario_s,
		s.ocodigopostal_s, s.rcvecctactual_s, s.rcveturno_s, s.onivel_s,
		CASE WHEN s.onivel_s = 'PJN' THEN 0
		WHEN s.onivel_s = 'PPR' THEN 1
		WHEN s.onivel_s = 'PES' THEN 2
		WHEN s.onivel_s = 'PST' THEN 3 END nivel,
		s.ogrado_s, s.opromedio_s, s.ocostoinscripcion_s, s.ocostocolegiatura_s,
		s.ofoliobeca_s, s.rcvesederecepcion_s, s.rcvefecharecepcion_s,
		s.rtipotutor_t, s.onombre_t as TNOM, s.oapellidopaterno_t as TAP,
		s.oapellidomaterno_t as TAM, s.oestadocivil_t, s.opercepcionmensual_t,
		s.ogastomensual_t, s.onumerodependientes_t, s.ogradoestudios_t,
		s.otipovivienda_t, s.otelefonofijo_t, s.otelefonocelular_t,
		s.oempleado_t, s.onombreempleo_t, s.ocomprobanteingresos_t, s.otipocomprobante_t
		FROM r3solicitantetutor_td_2 s
		WHERE concat (s.oapellidopaterno_s, s.oapellidomaterno_s) in
		(
			SELECT apt FROM
			(
				SELECT concat(s1.oapellidopaterno_s, s1.oapellidomaterno_s) apt
				FROM r3solicitantetutor_td_2 s1
				WHERE s1.odocumentacionentregada_s = 'S'
				AND s1.obeneficiado_s is null
				AND s1.ocancelado_s is null
				AND (s1.oposiblehermano_s is null OR s1.oposiblehermano_s <> 'R')
			)
			group by apt
			having count(apt) > 1
		)
		AND s.odocumentacionentregada_s='S'
		AND s.obeneficiado_s is null
		AND s.ocancelado_s is null
		AND (s.oposiblehermano_s is null OR s.oposiblehermano_s <>'R')
		ORDER BY s.oapellidopaterno_s, s.oapellidomaterno_s, s.onivel_s, s.ogrado_s";

        $rslH = $db_pdo->query($qry) or die("Query Posibles Hermanos Error... ". $qry);

        $marcado =0;
        while($rowH = $rslH->fetch(PDO::FETCH_ASSOC)){
            //cancelado por hermano(3)
            $qryH = "UPDATE r3solicitantetutor_td_2 
			SET oposiblehermano_s = 'S'
			WHERE kcvesolicitante = ".$rowH["KCVE"]." 
			AND oejercicio='2011'";

            $marcado = $marcado + $db_pdo->exec($qryH) or die($qryH);
        }
        return $marcado;

    }  
	
	function verificarPromedioPlaneacion($db_pdo){
	
		$qry = "SELECT s.kcvesolicitante as kcve, p.opromedio as pp
		FROM r3solicitantetutor_td_2 s, r3planeacion p
		WHERE (s.ocurp_s = p.ocurp)";
	
		$rsl = $db_pdo->query($qry) or die ($qry);
	
		$promverificado = 0;
		
		while($row = $rsl->fetch(PDO::FETCH_ASSOC)){
		
		
			//obtenemos el promedio de planeacion con un decimal y sin redondearlo
			$multiplicador = pow (10,1);
			$resultado = ((int)($row["PP"] * $multiplicador)) / $multiplicador;
			$prom =  number_format($resultado, 1);
		
		
			$q = "UPDATE r3solicitantetutor_td_2 
			SET opromedioverificado_s = " .$prom."
			WHERE kcvesolicitante = " . $row["KCVE"]."
			AND oejercicio='2011'";
	
			$promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error... ". $q);
			
		}
		
		
		
		$sql2 = "SELECT kcvesolicitante as kcve, opromedio_s, opromediocapturadoc_s as promdoc
		FROM r3solicitantetutor_td_2
		where overificacionplaneacion_s='R'
		AND opromediocapturadoc_s is not null
		AND opromediocapturadoc_s<>'-1.0'";
		$rslt2 = $db_pdo->query($sql2) or die ($sql2);
	
		while($row2 = $rslt2->fetch(PDO::FETCH_ASSOC)){
		
			$sqlup = "UPDATE r3solicitantetutor_td_2 
			SET opromedioverificado_s = " . $row2["PROMDOC"]."
			WHERE kcvesolicitante = " . $row2["KCVE"]."
			AND oejercicio='2011'";
			$rsltup = $db_pdo->query($sqlup) or die("Query PROMEDIOS rojos Error... ". $sqlup);
			$rowup = $rsltup->fetch(PDO::FETCH_ASSOC);
			
			$promverificado = $promverificado + 1;
		}
		return $promverificado;
	}
	
	
	  
    function verificarPromedio($db_pdo){
        
		$PROMEDIO_MIN = 8.5;

        $sqlsol = "SELECT s.kcvesolicitante as kcve, s.ogrado_s as grado, s.opromedioverificado_s as promv, s.onivel_s as nivel 
		FROM r3solicitantetutor_td_2 s
		WHERE s.obeneficiado_s is null
		AND s.ocancelado_s is null
		AND (s.oposiblehermano_s is null or s.oposiblehermano_s <>'R')";
        
        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Promedio Verificado Error! " . $sqlsol);

        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){

				if(($rowsol["NIVEL"] == "PES" || $rowsol["NIVEL"] == "PST" ||
                        ($rowsol["NIVEL"] == "PPR" && $rowsol["GRADO"] > 1))
                        && ($rowsol["PROMV"] != NULL && trim($rowsol["PROMV"]) != "")){
					
					if($rowsol["PROMV"] < $PROMEDIO_MIN){
						
					   	$sqlup = "UPDATE r3solicitantetutor_td_2 SET
                        ocancelado_s = 'S',
                        omotivocancelacion_s = 5
                        WHERE kcvesolicitante = ".$rowsol["KCVE"]."
						AND oejercicio='2011'";
						
                        $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Promedio Verificado Error! " . $sqlup);
						
                    }

                }
				
				
        }
        return $i;
    }
	
	
	  
	
	
	
	
    function asignarPonderacionGrado($db_pdo){
        
		$sqlsol = "SELECT kcvesolicitante as kcve, ogrado_s as ogrado, onivel_s as onivel
        FROM r3solicitantetutor_td_2
		WHERE oejercicio='2011'
		AND ofoliobeca_s is not null
		AND oponderaciongrado_s is null
		AND kcvesolicitante>15000";

        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Grado Error! " . $sqlsol);

        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
				
                $valor = 0;

                if($rowsol["OGRADO"] != NULL && trim($rowsol["OGRADO"]) != ""){
                    $sqlgdo = "SELECT ovalor 
					FROM r3ctponderaciongrado
                    WHERE onivel = '".$rowsol["ONIVEL"]."'
					AND ogrado = '" . $rowsol["OGRADO"] . "'
					AND oejercicio='2011'";
                    $rsltgdo = $db_pdo->query($sqlgdo) or die("Query Select Valor Ponderacion Grado Error!");
                    $rowgdo = $rsltgdo->fetch(PDO::FETCH_ASSOC);

                    $valor = $rowgdo["OVALOR"];
                }

                $sqlup = "UPDATE r3solicitantetutor_td_2 SET
				oponderaciongrado_s = ".$valor."
				WHERE kcvesolicitante = ".$rowsol["KCVE"]."
				AND oejercicio='2011'";
                //echo $sqlup . "<br/>";


                $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Grado Error! " . $sqlup);
				
        }
        return $i;
		
    }
	
	
  
    function asignarPonderacionPromedio($db_pdo){
		
        $sqlsol = "SELECT kcvesolicitante as kcve, opromedioverificado_s as pv, onivel_s as onivel, ogrado_s as ogrado
		FROM r3solicitantetutor_td_2
		WHERE oejercicio='2011'
		AND ofoliobeca_s is not null
		AND oponderacionpromedio_s is null
		AND kcvesolicitante>15000";
        
		$rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Promedio Error! " . $sqlsol);
		$i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
			
			$valor = 0;
			
			if(($rowsol["ONIVEL"] == "PES" || $rowsol["ONIVEL"] == "PST" ||($rowsol["ONIVEL"] == "PPR" && $rowsol["OGRADO"] > 1))
				&& ($rowsol["PV"] != NULL && $rowsol["PV"] > 0 && trim($rowsol["PV"]) != "")){
				
				$promedio = $rowsol["PV"] * 10;

				$sqlprom = "SELECT ovalor
				FROM r3ctponderacionpromedio
				WHERE opromedio = '" . $promedio . "'
				AND oejercicio='2011'";
				$rsltprom = $db_pdo->query($sqlprom) or die("Query Select Valor Ponderacion Promedio Error!");
				$rowprom = $rsltprom->fetch(PDO::FETCH_ASSOC);
				
				$valor = $rowprom["OVALOR"];
			}
			
			$sqlup = "UPDATE r3solicitantetutor_td_2 SET
			oponderacionpromedio_s = ".$valor."
			WHERE kcvesolicitante = " . $rowsol["KCVE"]."
			AND oejercicio='2011'";

			$i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Promedio Error! " . $sqlup."<br>".$sqlprom);
			
        }
		
        return $i;
		
    }
	
	
    function asignarPonderacionIngreso($db_pdo){
		
		$sqlsol = "SELECT kcvesolicitante as kcve, onivel_s as onivel, opercepcionmensual_t as pm
		FROM r3solicitantetutor_td_2
        WHERE oejercicio='2011'
		AND ofoliobeca_s is not null
		AND oponderacioningreso_s is null
		AND kcvesolicitante>15000";
	

        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Ingreso Error! " . $sqlsol);
        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
        	$valor = 0;

			if($rowsol["PM"] != NULL && trim($rowsol["PM"]) != ""){
				$sqlingreso = "SELECT ovalor
				FROM r3ctponderacioningreso
				WHERE onivel = '".$rowsol["ONIVEL"]."'
				AND rcveingreso = '".$rowsol["PM"]."'
				AND oejercicio='2011'";
				$rsltingreso = $db_pdo->query($sqlingreso) or die("Query Select Valor Ponderacion Ingreso Error!");
				$rowingreso = $rsltingreso->fetch(PDO::FETCH_ASSOC);
				
				$valor = $rowingreso["OVALOR"];
			}

			$sqlup = "UPDATE r3solicitantetutor_td_2 SET
			oponderacioningreso_s = ".$valor."
			WHERE kcvesolicitante = ".$rowsol["KCVE"]."
			AND oejercicio='2011'";

			$i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Ingreso Error! " . $sqlup);
			
        }
		
		
		
				
        return $i;
    }
	
	
	
	
    function asignarPonderacionTotal($db_pdo){
		$fecha = date('Y-m-d H:i:s');
		//NVL ("valor que puede tener null", "valor que tomaria en caso de tener null")
        $sql = "SELECT kcvesolicitante as kcve,
        NVL(oponderacioningreso_s, 0) ponderacioningreso,
		NVL(oponderacionpromedio_s, 0) ponderacionpromedio,
        NVL(oponderaciongrado_s, 0) ponderaciongrado
		FROM r3solicitantetutor_td_2  
		WHERE oejercicio='2011'
		AND ofoliobeca_s is not null
		AND obeneficiado_s is null
		AND kcvesolicitante>15000";

		$rslt = $db_pdo->query($sql) or die("Query Select Solicitante Ponderacion Total Error! " . $sql);
        $i = 0;
        while ($row = $rslt->fetch(PDO::FETCH_ASSOC)) {
			
            $ponderaciontotal = $row["PONDERACIONINGRESO"] + $row["PONDERACIONPROMEDIO"] + $row["PONDERACIONGRADO"];

            $sqlpond = "UPDATE r3solicitantetutor_td_2 SET
			oponderaciontotalinicial_s = '".$ponderaciontotal."',
            oponderaciontotalasignada_s = '".$ponderaciontotal."',
            istatus = 'M',
            ifecmod = '".$fecha."'
            WHERE kcvesolicitante = '".$row["KCVE"]."'
			ANd oejercicio='2011'";

            $i = $i + $db_pdo->exec($sqlpond) or die("Query Update Solicitante Ponderacion Total Error! " . $sqlpond);
			
        }
        return $i;
    }
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	  
	function calcular5porciento($db_pdo){
		
        $PORCIENTO = 0.05;
		/*
        $sql = "SELECT x.kcvect as kcvect, t.kcveturno as kcveturno, count(x.kcvect) as total
		FROM r3planeacion p, x1centrotrabajo x, x1ctturno t
		WHERE (p.occt=x.oclave)
		AND (t.odescripcion=p.oturno)
		GROUP BY x.kcvect, t.kcveturno
		ORDER BY x.kcvect, t.kcveturno";*/
		$sql ="SELECT c.kcvect as kcvect, c.oclave as cct, c.rcveturno as kcveturno
		FROM x1centrotrabajo c
		WHERE (c.ostatus = 1 OR c.ostatus = 4)
		AND (substr(c.oclave, 3,3)='PJN' or substr(c.oclave, 3,3)='PPR' or substr(c.oclave, 3,3)='PES' 
		or substr(c.oclave, 3,3)='PST')
		AND c.oclave NOT IN (SELECT clave FROM r3cctsconrevocacion)
		AND c.rcvelocalidad_inegi is not null
		AND c.oclave NOT IN ('15PST0036D','15PES0001W','15PES1416A','15PES1417Z','15PES1418Z',
		'15PES1419Y','15PES1420N','15PJN1463B','15PJN2155C','15PJN2499X','15PJN2668B','15PJN2725C',
		'15PJN2825B','15PJN2841T','15PJN2913W','15PJN2914V','15PJN2974J','15PJN3014K','15PJN3015J',
		'15PJN3016I','15PJN3017H','15PJN3018G','15PJN3019F','15PJN3020V','15PJN3021U','15PJN3022T',
		'15PJN3023S','15PJN3024R','15PJN3025Q','15PPR3705M','15PPR3736F','15PPR3749J','15PPR3752X',
		'15PPR3753W','15PPR3754V','15PPR3755U','15PPR3756T','15PPR3757S','15PPR3758R','15PPR3759Q',
		'15PPR3760F','15PPR3761E','15PPR3762D','15PPR3763C','15PJN2073T')
		order by c.kcvect";
		
        $rsltot = $db_pdo->query($sql) or die("Query Select totalCT Error!");
        $i = 0;
        while ($row = $rsltot->fetch(PDO::FETCH_ASSOC)) {
		
                $i = $i + 1;  
				
				$sqlb = "select count(*) as total FROM r3planeacion
				WHERE occt='".$row["CCT"]."'";
				$rsltb = $db_pdo->query($sqlb) or die ("Query error b...");
				$rowb = $rsltb->fetch(PDO::FETCH_ASSOC);
				
				
                $porcentajeAlumnos = $rowb["TOTAL"] * $PORCIENTO;
                $totalPtos = $porcentajeAlumnos * 100;
				
                $sqlin = "INSERT INTO r3capacidadbecas (rcvecct, rcveturno, ototalalumnos, ototalbecascompletas, 
				ototalpuntos, opuntosasignados, opuntosrestantes, oejercicio) 
				VALUES (" . $row["KCVECT"] . ",". $row["KCVETURNO"] . "," . $rowb["TOTAL"] . "," . $porcentajeAlumnos . ",
				" . $totalPtos . ",0," . $totalPtos .", '2011')";
				//echo $sqlin."<br><br>";
                $db_pdo->query($sqlin) or die("Query Insert r3Capacidad  Error! <br/>".$sqlin);

				
				  
        }  

        return $i;
	
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	
	function asignar($db_pdo, $porcentaje, $ronda){
        $PORCENTAJE_INI = 25;
        $PORCENTAJE_MIN = 10;
		     
		
    
        $sqlct = "";
        switch($ronda){
                case 1: $sqlct = "SELECT cap.kcvecapacidad, cap.rcvecct as rcvecct, x.oclave, cap.rcveturno as rcveturno,
				cap.ototalbecascompletas as ototalbecascompletas, cap.ototalpuntos as ototalpuntos,
				cap.opuntosasignados as opuntosasignados, cap.opuntosrestantes as opuntosrestantes,
				x.oclave as oclave, x.ofolioincorporacion as ofolioincorporacion, t.odescripcion as turno
				FROM r3capacidadbecas cap, x1centrotrabajo x, x1ctturno t
				WHERE (cap.rcvecct = x.kcvect)
				AND (cap.rcveturno = t.kcveturno)
				AND cap.oejercicio='2011'
				AND cap.ototalalumnos>=5
				AND cap.kcvecapacidad>=11929 AND cap.kcvecapacidad<12029
				order by cap.rcvecct";
				//cct de r3capacidadcita que tienen totaldealumnos mayor o igual a 5
                break;       
				
                case 2: $sqlct = "SELECT cap.rcvecct as rcvecct, cap.rcveturno as rcveturno,
				cap.ototalbecascompletas as ototalbecascompletas, cap.ototalpuntos as ototalpuntos,
				cap.opuntosasignados as opuntosasignados, cap.opuntosrestantes as opuntosrestantes,
				x.oclave as oclave, x.ofolioincorporacion as ofolioincorporacion, t.odescripcion as turno
				FROM r3capacidadbecas cap, x1centrotrabajo x, x1ctturno t
				WHERE (cap.rcvecct = x.kcvect)
				AND (cap.rcveturno = t.kcveturno)
				AND cap.oejercicio='2011'
				AND cap.ototalalumnos>=5
				AND opuntosrestantes>=".$PORCENTAJE_MIN."";
				//ccts de r3capacidadcita que tienen totaldealumnos mayor o igual a 5 y que tienen mas de 10 puntosrestantes

                break;
        }
		
		//echo $sqlct."<br>";
		
		
		
		/*
        //$sqlct = $sqlct . " AND x1ct.oclave NOT IN ('15PES1302Z','15PPR6501S','15PES1325J','15PPR3531M','15PJN2316Z','15PJN2232R','15PJN1646J', '15PES0644O', '15PPR2612Q')";
        $sqlct = $sqlct . " AND x1ct.oclave IN ('15PES1302Z','15PPR6501S','15PES1325J','15PPR3531M','15PJN2316Z','15PJN2232R','15PJN1646J', '15PES0644O', '15PPR2612Q')";
        //$sqlct = $sqlct . " AND kcvecapacidad <= 9300";
        //$sqlct = $sqlct . " AND kcvecapacidad > 9300 and kcvecapacidad <= 9500";
        //$sqlct = $sqlct . " AND kcvecapacidad > 9500 and kcvecapacidad <= 10100";
        $sqlct = $sqlct . " ORDER BY kcvecapacidad";


        //$sqlct = $sqlct . " AND kcvect = 9681";
        //$sqlct = $sqlct . " AND kcvect = 9447";
        //$sqlct = $sqlct . " AND kcvect = 9013";
        //$sqlct = $sqlct . " AND kcvect = 10440";
        //$sqlct = $sqlct . " AND kcvect = 10474";
		*/

		
		
		
        $rsltct = $db_pdo->query($sqlct) or die("Query Select1 Asignar Error! <br/>".$sqlct);

        $i = 0;
        $numreg = 0;
        while ($rowct = $rsltct->fetch(PDO::FETCH_ASSOC)) {
		
            $nivel = substr($rowct["OCLAVE"], 2, 3);
            $isAG = substr($rowct["OFOLIOINCORPORACION"], 0,2);

            $numreg = $numreg + 1;
			
			$puntos = 0;
            $numerobecas = 0;
            $qryPAsignados = "";
			
			
			
			
            switch($ronda){
                case 1: $puntos = $rowct["OTOTALPUNTOS"]; //total de puntos por cada cct
                        $porcentajebecas = ($porcentaje / 100) * $puntos; //sacamos el 80% de ese total de puntos
						
                        //verificar si ya se han repartido puntos de ese X1 %
						//en los casos de tramite directo es cuando ya se han ocupado algunos puntos
                        $qryPAsignados = "SELECT NVL(SUM(oporcentajeasignado_s), 0) as suma
                        FROM r3solicitantetutor_td_2
                        WHERE rcvecctactual_s = '".$rowct["RCVECCT"]."'
                        AND rcveturno_s = ".$rowct["RCVETURNO"]."
						AND oejercicio='2011'
                        AND obeneficiado_s = 'S'";
						
						
                        $rsltAsig = $db_pdo->query($qryPAsignados) or die ("Query Puntos AsignadosError!!.... ". $qryPAsignados);
                        $rowptosAsignados = $rsltAsig->fetch(PDO::FETCH_ASSOC);

                        if($rowptosAsignados["SUMA"] > 0){
                            $porcentajebecas = $porcentajebecas - $rowptosAsignados["SUMA"]; 
							//sera los que sobro despues de tramites derectos
                            $puntos = $porcentajebecas;
							
                        }
						
						
						
                break;
                case 2: $puntos = $rowct["OPUNTOSRESTANTES"]; //total de puntos son los puntosrestantes
                        $porcentajebecas = $puntos; //porcentaje para becas es lo mismo de los puntos restantes
                break;
            }
			
			
			 //echo "<br/>Porcentaje de Becas: " . $porcentajebecas . "<br/>";

            if($porcentajebecas >= $PORCENTAJE_MIN){ // si trae >= 10 puntos
                if($porcentajebecas > $PORCENTAJE_INI){ //si el porcentaje a asignar es mayor a 25
                    $residuo = $porcentajebecas % $PORCENTAJE_INI; //residuo es lo que sobra de dar todas las becas al 25%
                    $porcentajebecas = $porcentajebecas - $residuo; //porcentajea asignar es la cantidad exacta de becas al 25%
                }else{
                    if($puntos >= $PORCENTAJE_INI){
                        $residuo = $puntos % $PORCENTAJE_INI;//lo que sobra de dar los puntos a becas del 25%
                        $porcentajebecas = $puntos - $residuo;//lo que se asignara en becas exactas al 25
                    }else{
                        $residuo = $puntos;
					}	
                }

                //echo "<br/>Porcentaje de Becas2: " . $porcentajebecas . "<br/>";

                $numerobecas = floor($porcentajebecas / $PORCENTAJE_INI); //divide la cantidadde becas entre 25
				
				
                if($residuo >= $PORCENTAJE_MIN && $ronda == 2){ //si es la 2da. ronda (20%) y sobran 10 o mas
                    $numerobecas = $numerobecas + 1;
				}
				
                if($numerobecas >= 1){
                    $ben = $this->setCandidatos($rowct["RCVECCT"],$rowct["RCVETURNO"], $nivel, $isAG, $numerobecas, $db_pdo);
					
                    $i = $i + $ben;
				}	
				
            }
			
			
			
            //echo "<br/>Residuo: " . $residuo . "<br/>";
            //echo "<br/>" . $rowct['OTOTALPUNTOS'] . "<br/>";
            //echo "<br/>" . $porcentajebecas . "<br/>";
            //echo "Puntos asignados:" . $rowptosAsignados['SUMA'] . "<br/>";
            //echo "Numero de Becas: " . $numerobecas . "<br/><br/>";

            echo "reg: " . $numreg . " kvecct : " . $rowct["RCVECCT"] . " turno: " . $rowct["RCVETURNO"] . " beneficiados: " . $ben."<br/>";
			
			
        }
        return $i;

		
    } 
	
	
	
	
	function setCandidatos($kcct, $turno, $nivel, $isAG, $numerobecas, $db_pdo){
        $PORCENTAJE_INI = 25;

        $gradoini = 1;
        $grados = 0;
		
		
		
        if($nivel == "PJN" || $nivel == "PES" || $nivel == "PST"){
            $grados = 3;
            $gradosparticipantes = 3;
            //"En planteles con Acuerdo "AG", s�lo se beneficia a los alumnos
            //de segundo y tercer grado, ya que el primer grado no es obligatorio"
            if($nivel == "PJN" && $isAG == "AG"){
                $gradoini = 2;
                $gradosparticipantes = 2;
            }
        }else{
            $grados = 6;
            $gradosparticipantes = 6;
        }
		
		//PES PST y PJN
		//$gradoini=1;
		//$grados=3;
		//$gradosparticipantes=3;
		//PPR
		//$gradoini=1;
		//$grados=6;
		//$gradosparticipantes=6;
		//PJN is AG
		//$gradoini=2;
		//$grados=3;
		//$gradosparticipantes=2;
				
		
		
        $gradocantidad = $this->distribuir($numerobecas, $gradosparticipantes, $gradoini, $grados, $kcct, $turno, $db_pdo);

		
		//$this->printA($gradocantidad);//muestra el contenido del vector gradocantidad
		
		
        $sql = "";
		
        for($i=$gradoini; $i<=$grados; $i++){ //empieza recorrido 123456 dependiendo nivel si es AG 23
            if($gradocantidad[$i]["cantidad"] > 0){ // si al menos una beca por grado....
				
                if($i > 1 && !trim($sql) == ""){ //entra en la segunda posicion
                    $sql = $sql . " UNION";
				}	
                $sbqry = $this->queryCandidatosXcentroTrabajoGrado($gradocantidad[$i]["cantidad"], $kcct, $turno, $i);



                $sql = $sql . " SELECT a.kcvesolicitante as kcvesolicitante, a.ogrado_s as grado, a.oprioridad_s as prioridad, 
				a.oponderaciontotalasignada_s as ponderaciontotasig, a.opromedioverificado_s as promver
                FROM r3solicitantetutor_td_2 a, (". $sbqry .") b
                WHERE a.kcvesolicitante = b.kcvesolicitante";
                   
            }
        }
				
		
        if(!trim($sql) == ""){ 
			$sql = $sql . " ORDER BY grado ASC, promver DESC, ponderaciontotasig DESC, prioridad DESC";
		} 
		
		
		//echo $sql;//consulta kcve grado, prioridad, ponderaciontotalasignada y promedioverificado de los grados participantes
		//pero solo los que se tienen en [cantidad] dentro del vector
	
		
        
        $rslt = $db_pdo->query($sql);// or die("Query Select Union Error! <br/>hh" . $sql);

        //MARCAR A TODOS LOS CANDIDATOS
        $i = 0;
		if($rslt != NULL){
        	while ($row = $rslt->fetch(PDO::FETCH_ASSOC)) {
            	$i = $i + 1;

				$sqlcandi = "UPDATE r3solicitantetutor_td_2 SET
				obeneficiado_s = 'S',
				oposiblehermano_s = 'G'
				WHERE kcvesolicitante = '".$row["KCVESOLICITANTE"]."'";
	
				$db_pdo->query($sqlcandi) or die("Query Update Solicitante Candidato Error! " . $sqlcandi);

            	$this->asignarPorcentaje($db_pdo, $row["KCVESOLICITANTE"], $PORCENTAJE_INI, $kcct, $turno);
				
        	}
      	}
		
		return $i;
	  
    }
	
	
	
	
	
	function distribuir($numerobecas, $gradosparticipan, $gradoini, $gradostot, $kcct, $turno, $db_pdo){
	
        $cantidad = floor($numerobecas / $gradosparticipan); 
        $restantes = $numerobecas % $gradosparticipan;
        $b = 0;
        $cc = 0;

        for($k = $gradoini; $k<=$gradostot; $k++){
            $gradocantidad[$k]["grado"] = $k;
            $gradocantidad[$k]["cantidad"] = $cantidad;
			

            $conteo = $this->queryContarCandidatosXGrado($kcct, $turno, $k, $db_pdo); //obtenemos el numero de cantidatos xgrado

			$gradocantidad[$k]["conteo"] = $conteo;
          
		    
			if($conteo <= $cantidad){ // si los cantidatos son menores o iguales  ala cantidad de becas  xgrado
                $gradocantidad[$k]["status"] = 0;
            }else{
                $gradocantidad[$k]["status"] = 1;//loscandidatos son mayores a la cantidad de becas xgrado
				$cc = $cc + 1;
            }
			
			
			
        }
		
		
		
        for($i = $gradostot; $i >= $gradoini; $i--){ //decrementa  654321
			
			
            if($gradocantidad[$i]["cantidad"] > $gradocantidad[$i]["conteo"]){ //si la cantidad de beca es mayor a los candidatos
				
			
                $b = 1;
                $cantidad = $gradocantidad[$i]["cantidad"];
                $gradocantidad[$i]["cantidad"] = $gradocantidad[$i]["conteo"];

                $faltantes = $cantidad - $gradocantidad[$i]["conteo"];
                $nvacantidad = $faltantes + $restantes;
                $restantes = 0;
				
                for ($h = $gradostot; $h >= $gradoini; $h--){//decrementa 654321
					
                    if($gradocantidad[$h]["status"] == 1 && $nvacantidad > 0){
                        $gradocantidad[$h]["cantidad"] = $gradocantidad[$h]["cantidad"] + 1;
                        $nvacantidad = $nvacantidad - 1;

                        if($gradocantidad[$h]["conteo"] == $gradocantidad[$h]["cantidad"]){
                            $gradocantidad[$h]["status"] = 0;
                            $cc = $cc - 1;
                        }
                    }
                    if($nvacantidad > 0 && $h == $gradoini && $cc > 0) $h = $gradostot + 1;
					//hace que el for inicie de nuevo la regresion
				
                }
				
				
				
				
            }
			
        }
		
		
		
        if($restantes > 0 && $b == 0){
			$nvacantidad = $restantes;
			for ($h = $gradostot; $h >= $gradoini; $h--) {//empieza dremento 654321
				if($gradocantidad[$h]["status"] == 1 && $nvacantidad > 0){
					$gradocantidad[$h]["cantidad"] = $gradocantidad[$h]["cantidad"] + 1;
					$nvacantidad = $nvacantidad - 1;
					if($gradocantidad[$h]["conteo"] == $gradocantidad[$h]["cantidad"]){
						$gradocantidad[$h]["status"] == 0;
						$cc = $cc - 1;
					}
				}
				if($nvacantidad > 0 && $h == $gradoini && $cc > 0) $h = $gradostot + 1;
			}
        }
		
		
        return $gradocantidad;
		
    }
	
	
	
	function queryCandidatosXcentroTrabajoGrado($numerobecas, $kcct, $turno, $grado){
		//obtiene query 
		//ordena los candidatos para becas
		//ordenandolos iniciando con los promedios mas altos, 
		//despues las ponderacionestotales asignadas mas altas
		//y las prioridades ordenadas por las mas altas
        $sbqry = "SELECT FIRST ".$numerobecas." s.kcvesolicitante
		FROM r3solicitantetutor_td_2 s, r3controlstatus c
        WHERE (s.kcvesolicitante = c.rcvesolicitante)
        AND s.rcvecctactual_s = '".$kcct."'
        AND s.rcveturno_s = '".$turno."'
        AND c.ofechaentrega IS NOT NULL
		AND s.ocancelado_s IS NULL
		AND s.oposiblehermano_s IS NULL
		AND s.odocumentacionentregada_s='S'
		AND s.obeneficiado_s IS NULL
        AND s.oejercicio='2011'
        AND s.ogrado_s = '".$grado."'
		ORDER BY opromedioverificado_s DESC, oponderaciontotalasignada_s DESC, oprioridad_s DESC";
        
		
		
        return $sbqry;
    }
	
	
	function queryContarCandidatosXGrado($kcct, $turno, $grado, $db_pdo){
        //cuenta los candidatos que tenemos en cierto cct  de acuerdo a un grado en especifico
		//los candidatos deben tener registro completo
		//-no deben estar cancelados
		//-no deben ser posible hermano
		//-deben tener la documentacion entregada
		//-no deben ser beneficiados
 
        $sbqry = "SELECT count(s.kcvesolicitante)
		FROM r3solicitantetutor_td_2 s, r3controlstatus c
		WHERE (s.kcvesolicitante = c.rcvesolicitante)
		AND s.rcvecctactual_s = '".$kcct."'
		AND s.rcveturno_s = '".$turno."'
		AND c.ofechaentrega IS NOT NULL
		AND s.ocancelado_s IS NULL
		AND s.oposiblehermano_s IS NULL
		AND s.odocumentacionentregada_s='S'
		AND s.obeneficiado_s IS NULL
		AND s.oejercicio='2011'
		AND s.ogrado_s = '".$grado."'"; 

        $rslt1 = $db_pdo->query($sbqry) or die ("Query distribuir Gradio Error!!.... ". $sbqry);
		$num = $rslt1->fetchColumn();

        return $num;
    }
	
	
	
	function asignarPorcentaje($db_pdo, $kcvesol, $porcentaje, $kcct, $turno){
    	//--realiza update en oporcentajeasignado, isurmodprc='PROCESO' y  ifecmodprc.
		//--encaso de que el porcentaje a asignar sobrepase a los puntos restantes el 
		//  porcentaje se queda con los puntos restantes que tenemos.
		//--actualizamos r3capacidadbeca opuntosasignados y puntosrestantes.
		
	    $Fec = date('Y-m-d H:i:s');

        $qryPuntos = "SELECT opuntosrestantes, opuntosasignados as pa
        FROM r3capacidadbecas
        WHERE rcvecct=".$kcct."
        AND rcveturno=".$turno."
		AND oejercicio=2011";
        $rPtos = $db_pdo->query($qryPuntos) or die("Query Puntos capacidad becas Error!! " . $qryPuntos);
		
        $row = $rPtos->fetch(PDO::FETCH_ASSOC);
        $ptos = $row["OPUNTOSRESTANTES"];
		$ptosAsignados = $row["PA"];

        if($ptos <= $porcentaje){
            $porcentaje = $ptos;
		} 
		
		
        $sql = "UPDATE r3solicitantetutor_td_2 SET
        oporcentajeasignado_s = ".$porcentaje.",
		iusrmodprc_s = 'PROCESO',
        ifecmodprc_s = '".$Fec."'
        WHERE kcvesolicitante =".$kcvesol."
		AND oejercicio='2011'";
        $db_pdo->query($sql) or die("Upadte Solicitante porcentaje Error!! ... " . $sql);

        $restante = $ptos - $porcentaje;
		$asignado = $ptosAsignados + $porcentaje;

        $qryup = "UPDATE r3capacidadbecas SET
        opuntosrestantes = ".$restante.",
		opuntosasignados = ".$asignado."
        WHERE rcvecct = ".$kcct."
        AND rcveturno = " . $turno."
		AND oejercicio='2011'";
        
        $db_pdo->query($qryup) or die("UPDATE CapacidadBecas puntos restantes Error!! ".$qryup);
    }
	

    function printA($g){
		//muestra los valores que se tienen de gradocantidad [grado][cantidadbecas][candidatosxgrado][dbpdo]
        foreach ($g as $value) {
                echo $value["grado"] . " : ";
                echo $value["cantidad"] . " : ";
                echo $value["conteo"] . " : ";
                echo $value["status"] . "<br/>";
        }
    }
    
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
?>