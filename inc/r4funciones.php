<?php


        



function getEstado($cveEstado, $db_pdo){
    $sql = "SELECT onombre";
    $sql = $sql . " FROM x1ctentidadfederativa";
    $sql = $sql . " WHERE kcveentidadfederativa = '". $cveEstado ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_ASSOC);
    return $row['ONOMBRE'];
}
function getMunicipio($cveMunicipio, $db_pdo){
    $sql = "SELECT onombre";
    $sql = $sql . " FROM x1ctmunicipio";
    $sql = $sql . " WHERE kcvemunicipio= '". $cveMunicipio ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_ASSOC);
    return $row['ONOMBRE'];
}
function getMunicipioCCT($CCT, $db_pdo){
    $sql = "SELECT rcvemunicipio_sep";
    $sql = $sql . " FROM x1centrotrabajo";
    $sql = $sql . " WHERE kcvect= '". $CCT ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_NUM);
    return getMunicipio($row[0], $db_pdo);
}
function getLocalidad($cvelocalidad, $db_pdo){
    $sql = "SELECT onombre";
    $sql = $sql . " FROM x1ctlocalidad";
    $sql = $sql . " WHERE kcvelocalidad= '". $cvelocalidad ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_ASSOC);
    return $row['ONOMBRE'];
}
function getCTInfo($CCT, $db_pdo){
    $sql = "SELECT oclave, onombre_ct";
    $sql = $sql . " FROM x1centrotrabajo";
    $sql = $sql . " WHERE kcvect= '". $CCT ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_ASSOC);
    return $row;
}
function getTurno($cveTurno, $db_pdo){
    $sql = "SELECT odescripcion";
    $sql = $sql . " FROM x1ctturno";
    $sql = $sql . " WHERE kcveturno= '". $cveTurno ."'";

    $rslt = $db_pdo->query($sql);
    $row = $rslt->fetch(PDO::FETCH_ASSOC);
    return $row['ODESCRIPCION'];
}?>
<?php function getHtmlCombo($array, $opcionVal){
    foreach ($array as $value){
        $option = "<option value=" . $value["K"];
        if($opcionVal == $value["K"]){$option = $option . " selected=selected";}
        $option = $option . ">" . $value["V"] . "</option>";
        echo $option;
    }
}?>
<?php function getHtmlRadio($array, $opcionVal, $nombre, $onclickEvent){
    
    foreach ($array as $value){
        $option = "<label>";
        $option = $option . "<input type=\"radio\" name=\"" . $nombre . "\" value=\"" . $value["K"] . "\"";
        $option = $option . " id= \"" . $nombre.$value["K"] . "\"";
        if($opcionVal == $value["K"]){
            $option = $option . " checked='checked' ";
        }
        $option = $option . " onclick=\"" . $onclickEvent . "\"";
        $option = $option . "/>" . $value["V"] . "</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        echo $option;
    }
    
}?>
<?php function getHtmlRadioV($array, $opcionVal, $nombre, $onclickEvent){
    foreach ($array as $value){
        $option = "<label>";
        $option = $option . "<input type=\"radio\" name=\"" . $nombre . "\" value=\"" . $value["K"] . "\"";
        $option = $option . " id= \"" . $nombre.$value["K"] . "\"";
        if($opcionVal == $value["K"]){
            $option = $option . " checked='checked' ";
        }
        $option = $option . " onclick=\"" . $onclickEvent . "\"";
        $option = $option . "/>" . $value["V"] . "</label>";
        $option = $option . "<br>";

        echo $option;
    }
    
}?>
<?php function getHtmlRadioV2($array, $opcionVal, $nombre, $onclickEvent){
    
    $i = 0;
    foreach ($array as $value){
        $i = $i + 1;
        $option = "<label>";
        $option = $option . "<input type=\"radio\" name=\"" . $nombre . "\" value=\"" . $value["K"] . "\"";
        $option = $option . " id= \"" . $nombre.$value["K"] . "\"";
        if($opcionVal == $value["K"]){
            $option = $option . " checked='checked' ";
        }
        $option = $option . " onclick=\"" . $onclickEvent . "\"";
        $option = $option . "/>" . $value["V"] . "</label>&nbsp;&nbsp;&nbsp;";
        if($i % 4  == 0){ $option = $option . "<br>";}


        echo $option;
    }
}?>
<?php function getOpcionVal($array, $opcionK){
    $val = "";
    foreach ($array as $value){
        if($opcionK == $value["K"]){
            //echo strtoupper  ($value["V"]);
            $val = $value["V"];
            break;
        }
    }
    return $val;
}?>
<?php 
function validarVariables($cadenaVariables){

	$arregloVariables = explode(",",$cadenaVariables);
	
	for($i=0;$i<count($arregloVariables);$i++){
		
		if($_POST[$arregloVariables[$i]]==NULL || trim($_POST[$arregloVariables[$i]])=="" || $_POST[$arregloVariables[$i]]==-1){
			return false;
		}
	}
	
	return true;
}
function revisarCadena($cadena){
    $caracteres = array("\'", "\"", "\\");
    $cad = str_replace($caracteres, "", $cadena);
    return $cad;
}
function revisarNumero($numero){
    $caracteres = array("\'", "\"", "\\",".", ",");
    $num = str_replace($caracteres, "", $numero);
    return $num;
}

function revisarNumeroFloat($numero){
    $caracteres = array("\'", "\"", "\\",",");
    $num = str_replace($caracteres, "", $numero);
    return $num;
}
?>
<?php //---------------------------------------- Plantilla Get Grupos, Grados... Horarios.... ------------------
function getGrupos($db_pdo, $usugdo, $grado, $idtabla, $opcion){
$qryGrupos = "SELECT kcvegrupo, ogrupo";
$qryGrupos = $qryGrupos . " FROM r2grupo";
$qryGrupos = $qryGrupos . " WHERE rcveusuariogrado = " . $usugdo;
$qryGrupos = $qryGrupos . " AND istatus <> 'B'";

$rsltGpos = $db_pdo->query($qryGrupos) or die("Query Error... ");


$slctGrupos = "<select style='font-family: arial; font-size: 10px;' onchange='getHorarioIni(this,". $grado .");' id='gpo".$idtabla."' name='gpo".$idtabla."'>";
$slctGrupos = $slctGrupos . "<option value='0'>GPO</option>";
while($rowGpos = $rsltGpos->fetch(PDO::FETCH_ASSOC)){
    $slctGrupos = $slctGrupos . "<option value='" . $rowGpos['OGRUPO'] . "' ";
    if($rowGpos['OGRUPO'] == $opcion)
        $slctGrupos = $slctGrupos . "selected=selected";
    $slctGrupos = $slctGrupos . ">". $rowGpos['OGRUPO'] ."</option>";
}
$slctGrupos = $slctGrupos . "</select>";

return $slctGrupos;
}
function getMaterias($db_pdo, $grado, $nivel, $idtabla, $opcion){

    if($nivel == 'PES' || $nivel == 'PST'){
        $nivel = "SEC";
        $qryMaterias = "SELECT kcvemateria, onombre";
        $qryMaterias = $qryMaterias . " FROM r2materia";
        $qryMaterias = $qryMaterias . " WHERE ogrado = " . $grado;
        $qryMaterias = $qryMaterias . " ANd onivel = '" . $nivel."'";
    }else{
        $qryMaterias = "SELECT kcvemateria, onombre";
        $qryMaterias = $qryMaterias . " FROM r2materia";
        $qryMaterias = $qryMaterias . " WHERE ogrado is null";
        $qryMaterias = $qryMaterias . " ANd onivel is null";
        $qryMaterias = $qryMaterias . " ANd ocurricular = 'S'";
    }

    $rsltMaterias = $db_pdo->query($qryMaterias) or die("Query Error... ");

    $qryExtra = "SELECT kcvemateria, onombre";
    $qryExtra = $qryExtra . " FROM r2materia";
    $qryExtra = $qryExtra . " WHERE ocurricular = 'N'";

    $rsltExtra = $db_pdo->query($qryExtra) or die("Query Extra Error... ".$qryExtra);

    $slctMaterias = "<select style='font-family: arial; font-size: 9px;' id='mat".$idtabla."' name='mat".$idtabla."'>";
    $slctMaterias = $slctMaterias . "<option value='0'>- MATERIA -</option>";
    while($rowMaterias = $rsltMaterias->fetch(PDO::FETCH_ASSOC)){
        $slctMaterias = $slctMaterias . "<option value='" . $rowMaterias['KCVEMATERIA'] . "' ";
        if($rowMaterias['KCVEMATERIA'] == $opcion)
            $slctMaterias = $slctMaterias . "selected=selected";
            $slctMaterias = $slctMaterias . ">". htmlentities($rowMaterias['ONOMBRE']) ."</option>";
    }

    while($rowExtra = $rsltExtra->fetch(PDO::FETCH_ASSOC)){
        $slctMaterias = $slctMaterias . "<option value='" . $rowExtra['KCVEMATERIA'] . "' ";
        if($rowExtra['KCVEMATERIA'] == $opcion)
            $slctMaterias = $slctMaterias . "selected=selected";
            $slctMaterias = $slctMaterias . ">* ". htmlentities($rowExtra['ONOMBRE']) ."</option>";
    }

    $slctMaterias = $slctMaterias . "</select>";

    return $slctMaterias;
}
function gethorario($db_pdo, $usugdo, $grupo, $ini_fin, $opcionhr, $opcionmin){
    $qryhr = "SELECT kcvegrupo, ohorainicial, ohorafinal";
    $qryhr = $qryhr . " FROM r2grupo";
    $qryhr = $qryhr . " WHERE rcveusuariogrado = " . $usugdo;
    $qryhr = $qryhr . " AND ogrupo = '" . $grupo ."'";
    $qryhr = $qryhr . " AND istatus <> 'B'";

    

    $rslthr = $db_pdo->query($qryhr) or die("Query Error... ");
    $rowhr = $rslthr->fetch(PDO::FETCH_ASSOC);

    $banhr = 0;
if($rowhr['OHORAINICIAL'] != NULL && $rowhr['OHORAFINAL'] !=  NULL){
    $h1 = strtotime($rowhr['OHORAINICIAL']);
    $h2 = strtotime($rowhr['OHORAFINAL']);

    $hrini = date('H', $h1);
    $hrfin = date('H', $h2);

    $banhr = 1;
 }else{
     $hrini = 0;
     $hrfin = -1;
 }

    $slctHr = "<select style='font-family: arial; font-size: 10px;' id='hr".$ini_fin."' name='hr".$ini_fin."'>";
    $slctHr = $slctHr . "<option value='0'>Hr</option>";

    

    if($grupo != '0')
        for($i = $hrini; $i<=$hrfin; $i++){
            $slctHr = $slctHr . "<option value='" . str_pad($i,2,"0",STR_PAD_LEFT) . "' ";
            if(str_pad($i,2,"0",STR_PAD_LEFT) == $opcionhr)
                    $slctHr = $slctHr . "selected=selected";
            $slctHr = $slctHr . ">". str_pad($i, 2,"0",STR_PAD_LEFT) ."</option>";
        }
    $slctHr = $slctHr . "</select>";



    $slctHr = $slctHr . "<select style='font-family: arial; font-size: 10px;' id='min".$ini_fin."' name='mn".$ini_fin."'>";
    $slctHr = $slctHr . "<option value='0'>Min</option>";
    if($grupo != '0' && $banhr==1)
        for($j = 0; $j<60; $j= $j+15){
            $slctHr = $slctHr . "<option value='" . str_pad($j,2,"0",STR_PAD_LEFT) . "' ";
            if(str_pad($j,2,"0",STR_PAD_LEFT) == $opcionmin)
                    $slctHr = $slctHr . "selected=selected";
            $slctHr = $slctHr . ">". str_pad($j, 2,"0",STR_PAD_LEFT) ."</option>";
        }
    $slctHr = $slctHr . "</select>";

    $slctHr = $slctHr . "<input type='hidden' id='kgp".$ini_fin."' name='kgp".$ini_fin."' value='".$rowhr['KCVEGRUPO']."'>";


    return $slctHr;
}
function getCargos($db_pdo, $opcion, $usr){
    

    $qryCargo = "SELECT kcvectcargo, onombre";
    $qryCargo = $qryCargo . " FROM r2ctcargo";
    $qryCargo = $qryCargo . " ORDER BY onombre";

    $rsltCargo = $db_pdo->query($qryCargo) or die("query error cargos... ");

    $i = 0;
    $sel = 0;
    $slctCargo = "<select id='sel_cargo' name='sel_cargo' onchange='verificaCargo(this,".$usr.");'>";
    $slctCargo = $slctCargo . "<option value='0'>-- CARGO --</option>";
    while($rowCargo = $rsltCargo->fetch(PDO::FETCH_ASSOC)){
        $i = $i+1;
        $slctCargo = $slctCargo . "<option value='" . $rowCargo['KCVECTCARGO'] . "' ";
        if($rowCargo['KCVECTCARGO'] == $opcion){
            $slctCargo = $slctCargo . "selected=selected";
            $sel = $i;
        }
            $slctCargo = $slctCargo . ">". htmlentities($rowCargo['ONOMBRE']) ."</option>";
    }
    $slctCargo = $slctCargo . "</select>";
    $slctCargo = $slctCargo . "<input type='hidden' name='index' id='index'";
    $slctCargo = $slctCargo . "value='".$sel."'";
    $slctCargo = $slctCargo . "/>";
    return $slctCargo;
}
function getGradoEstudios($db_pdo, $opcion){
    $qry = "SELECT kcvegradoestudio, ogradoestudio";
    $qry = $qry . " FROM r2ctgradomaximoestudio";
    

    $rslt = $db_pdo->query($qry) or die("query error cargos... ");

    
    $slct = "<select id='sel_grado_maximo' name='sel_grado_maximo'>";
    $slct = $slct . "<option value='-1'>-- GRADO DE ESTUDIOS --</option>";
    while($row = $rslt->fetch(PDO::FETCH_ASSOC)){
        $slct = $slct . "<option value='" . $row['KCVEGRADOESTUDIO'] . "' ";
        if($row['KCVEGRADOESTUDIO'] == $opcion)
            $slct = $slct . "selected=selected";
            $slct = $slct . ">". htmlentities($row['OGRADOESTUDIO']) ."</option>";
    }
    $slct = $slct . "</select>";
    return $slct;
}
function getDocAcredita($db_pdo, $opcion){
    $qry = "SELECT kcvedocumentoacredita, odocumentoacredita";
    $qry = $qry . " FROM r2ctdocumentoacredita";
    

    $rslt = $db_pdo->query($qry) or die("query error cargos... ");

    $slct = "<select id='docacreditacion' name='docacreditacion' onchange='muestra(this);'>";
    $slct = $slct . "<option value='0'>-- DOCUMENTO --</option>";
    while($row = $rslt->fetch(PDO::FETCH_ASSOC)){
        $slct = $slct . "<option value='" . $row['KCVEDOCUMENTOACREDITA'] . "' ";
        if($row['KCVEDOCUMENTOACREDITA'] == $opcion)
            $slct = $slct . "selected=selected";
            $slct = $slct . ">". htmlentities($row['ODOCUMENTOACREDITA']) ."</option>";
    }
    $slct = $slct . "</select>";
    return $slct;
}
function isInCargo($cargo){
//----- PK de r2cargo que no puden estar frente a grupo ----
    $cargoNoFaG = array(36,33,32,9,29,30,5,35,37,8,34,4,1);
    if(in_array($cargo, $cargoNoFaG))
        return "N";
    else
        return "S";
}


//*****************************************
//Funci�n Para imprimir dia mes y a�o
//*****************************************

function imprimefecha($dia, $mes, $ano){
	$imprime = $imprime . "<select name='".$dia."' id='".$dia."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";
	$imprime = $imprime . "<option value='01'>01</option>";
	$imprime = $imprime . "<option value='02'>02</option>";
	$imprime = $imprime . "<option value='03'>03</option>";
	$imprime = $imprime . "<option value='04'>04</option>";
	$imprime = $imprime . "<option value='05'>05</option>";
	$imprime = $imprime . "<option value='06'>06</option>";
	$imprime = $imprime . "<option value='07'>07</option>";
	$imprime = $imprime . "<option value='08'>08</option>";
	$imprime = $imprime . "<option value='09'>09</option>";
	$imprime = $imprime . "<option value='10'>10</option>";
	$imprime = $imprime . "<option value='11'>11</option>";
	$imprime = $imprime . "<option value='12'>12</option>";	
	$imprime = $imprime . "<option value='13'>13</option>";	
	$imprime = $imprime . "<option value='14'>14</option>";	
	$imprime = $imprime . "<option value='15'>15</option>";	
	$imprime = $imprime . "<option value='16'>16</option>";	
	$imprime = $imprime . "<option value='17'>17</option>";	
	$imprime = $imprime . "<option value='18'>18</option>";	
	$imprime = $imprime . "<option value='19'>19</option>";	
	$imprime = $imprime . "<option value='20'>20</option>";	
	$imprime = $imprime . "<option value='21'>21</option>";	
	$imprime = $imprime . "<option value='22'>22</option>";	
	$imprime = $imprime . "<option value='23'>23</option>";	
	$imprime = $imprime . "<option value='24'>24</option>";	
	$imprime = $imprime . "<option value='25'>25</option>";	
	$imprime = $imprime . "<option value='26'>26</option>";	
	$imprime = $imprime . "<option value='27'>27</option>";	
	$imprime = $imprime . "<option value='28'>28</option>";	
	$imprime = $imprime . "<option value='29'>29</option>";	
	$imprime = $imprime . "<option value='30'>30</option>";	
	$imprime = $imprime . "<option value='31'>31</option>";	
	$imprime = $imprime . "</select>";
	$imprime = $imprime . " / <select name='".$mes."' id='".$mes."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	$imprime = $imprime . "<option value='01'>ENERO</option>";
	$imprime = $imprime . "<option value='02'>FEBRERO</option>";
	$imprime = $imprime . "<option value='03'>MARZO</option>";
	$imprime = $imprime . "<option value='04'>ABRIL</option>";
	$imprime = $imprime . "<option value='05'>MAYO</option>";
	$imprime = $imprime . "<option value='06'>JUNIO</option>";
	$imprime = $imprime . "<option value='07'>JULIO</option>";
	$imprime = $imprime . "<option value='08'>AGOSTO</option>";
	$imprime = $imprime . "<option value='09'>SEPTIEMBRE</option>";
	$imprime = $imprime . "<option value='10'>OCTUBRE</option>";
	$imprime = $imprime . "<option value='11'>NOVIEMBRE</option>";		
	$imprime = $imprime . "<option value='12'>DICIEMBRE</option>";										
	$imprime = $imprime . "</select>";
	$imprime = $imprime . " / <select name='".$ano."' id='".$ano."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($j=1995;$j<=2008;$j++){
		$imprime = $imprime . "<option value='".$j."'>".$j."</option>";
	}                 
	$imprime = $imprime . "</select>"; 
	
	return $imprime; 
}
	
//*****************************************
//Funci�n Para imprimir select de cantidades
//*****************************************
function imprimecantidad($nombre_cantidad){
	$imprime = "<select name='".$nombre_cantidad."' id='".$nombre_cantidad."' title='Selecciona cantidad'>";
	$imprime = $imprime . "<option value=''>-CANTIDAD-</option>";
	for($i=0;$i<=100;$i++){
		$imprime = $imprime . "<option value='".$i."'>".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	return $imprime; 
}


//*****************************************
//Funci�n Para imprimir select de cantidades, validando con campo de BD
//***************************************** 
function imprimecantidad2($nombre_cantidad, $cantidadBD){
	$imprime = "<select name='".$nombre_cantidad."' id='".$nombre_cantidad."' title='Selecciona cantidad'>";
	$imprime = $imprime . "<option value=''>-CANTIDAD-</option>";
	for($i=0;$i<=100;$i++){
		if($i == $cantidadBD){
			$seleccionado = " selected ";
		}else{
			$seleccionado = "";
		}
		
		$imprime = $imprime . "<option value='".$i."' ".$seleccionado.">".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	return $imprime; 
}



 
//*************************************************************
//Funci�n Para imprimir select dia mes ano validadndo con bd //
//*************************************************************
function imprimefecha2($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB){
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3' align='left'>";
	$imprime = $imprime . "<select name='".$dia5."' id='".$dia5."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";


	for($i=01;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionadoa = " selected ";
		}else{
			$seleccionadoa = " ";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionadoa." >".$i."</option>";
	}									
	$imprime = $imprime . "</select>";

	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " / <select name='".$mes5."' id='".$mes5."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " / <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
        
            
            
        $day = date("d");
                
                
        if(($day>='8') && ($day<='17')){
            //PJN
            $fecinic = '2013';
            $fecfin = '2009';
                
        }else{
            if((($day>='18') && ($day<='31')) || $day=='1'){
                //PPR
                $fecinic = '2010';
                $fecfin = '2002';       
            }else{
                if($day=='2' || $day=='3' || $day=='4'){
                    //PES
                    $fecinic = '2005';
                    $fecfin = '1998';
                }else{        
                    //PES        
                    //no entra en ningun periodo
                    $fecinic = '2013';
                    $fecfin = '2009';
                        
                }          
            }
        }       
        
                    
                
                
            
            
	
	for($k=$fecinic;$k>=$fecfin;$k--){
                
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select></td>"; 
		//$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}

    

        

function imprimefechaAdministrador($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB){
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3' align='left'>";
	$imprime = $imprime . "<select name='".$dia5."' id='".$dia5."' title='Selecciona dia'  >";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";

        
	for($i=01;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionadoa = " selected ";
		}else{
			$seleccionadoa = " ";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionadoa." >".$i."</option>";
	}									
	$imprime = $imprime . "</select>";

	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " / <select name='".$mes5."' id='".$mes5."' title='Selecciona mes' >";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " / <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o' >";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=2013;$k>=1989;$k--){
                    
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select></td>"; 
		//$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}

        
    
    

    
function imprimefechaT($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB){
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3' align='left'>";
	$imprime = $imprime . "<select name='".$dia5."' id='".$dia5."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";


	for($i=01;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionadoa = " selected ";
		}else{
			$seleccionadoa = " ";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionadoa." >".$i."</option>";
	}									
	$imprime = $imprime . "</select>";

	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " / <select name='".$mes5."' id='".$mes5."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " / <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1996;$k>=1920;$k--){
            
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                    
	$imprime = $imprime . "</select></td>"; 
		//$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}



function imprimefechaADM($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB){
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3'>";
	$imprime = $imprime . "<select name='".$dia5."' id='".$dia5."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";


	for($i=01;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionadoa = " selected ";
		}else{
			$seleccionadoa = " ";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionadoa." >".$i."</option>";
	}									
	$imprime = $imprime . "</select>";

	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " / <select name='".$mes5."' id='".$mes5."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " / <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1920;$k<=2010;$k++){
	
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select></td>"; 
		$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}





//*************************************************************
//Funci�n Para imprimir select dia mes ano validadndo con bd  333//
//*************************************************************
function imprimefecha3($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB, $blocked){

 
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3'>";
	$imprime = $imprime . "D&iacute;a: <select name='".$dia5."' id='".$dia5."' title='Selecciona dia' '".$blocked."'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";
	for($i=1;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionado = " selected ";
		}else{
			$seleccionado = "";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionado.">".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " Mes: <select name='".$mes5."' id='".$mes5."' title='Selecciona mes' '".$blocked."'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " A&ntilde;o: <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o' '".$blocked."'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1920;$k<=2025;$k++){
	
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select></td>"; 
		$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}




//*************************************************************
//Funci�n Para imprimir dos fechas validando con bd //
//*************************************************************
function imprime2fechas($etiqueta, $nom_dia1, $dia1DB, $nom_mes1, $mes1DB, $nom_ano1, $ano1DB, $nom_dia2, $dia2DB, $nom_mes2, $mes2DB, $nom_ano2, $ano2DB ){
	$imprime = "<td width='35%' class='tabtits1'>".$etiqueta."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3'>";
	$imprime = $imprime . "<b>DEL</b>  D&iacute;a: <select name='".$nom_dia1."' id='".$nom_dia1."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";
	for($i=1;$i<=31;$i++){
		if($i == $dia1DB){
			$seleccionado = " selected ";
		}else{
			$seleccionado = "";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionado.">".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " Mes: <select name='".$nom_mes1."' id='".$nom_mes1."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mes1DB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " A&ntilde;o: <select name='".$nom_ano1."' id='".$nom_ano1."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1920;$k<=2025;$k++){
	
		if($k == $ano1DB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select><br />  <b>AL</b>";
	
	
	
	
	
	 
	
	
	
	
	$imprime = $imprime . " D&iacute;a: <select name='".$nom_dia2."' id='".$nom_dia2."' title='Selecciona dia'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";
	for($i=1;$i<=31;$i++){
		if($i == $dia2DB){
			$seleccionado = " selected ";
		}else{
			$seleccionado = "";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionado.">".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " Mes: <select name='".$nom_mes2."' id='".$nom_mes2."' title='Selecciona mes'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mes2DB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " A&ntilde;o: <select name='".$nom_ano2."' id='".$nom_ano2."' title='Selecciona a&ntilde;o'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1920;$k<=2025;$k++){
	
		if($k == $ano2DB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select></td>"; 
	$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}




//*************************************************************
//Funci�n Para imprimir select dia mes ano validadndo con bd //
//*************************************************************
function imprimefecha222($nombre5, $dia5, $diaDB, $mes5, $mesDB, $ano5, $anoDB, $color){
	$imprime = "<td width='35%' class='tabtits1'>".$nombre5."</td>";
	$imprime = $imprime . "<td width='65%' bgcolor='#F3F3F3'>";
	$imprime = $imprime . "D&iacute;a: <select name='".$dia5."' id='".$dia5."' title='Selecciona dia' style='background:".$color."'>";
	$imprime = $imprime . "<option value=''>-D&Iacute;A-</option>";
	for($i=1;$i<=31;$i++){
		if($i == $diaDB){
			$seleccionado = " selected ";
		}else{
			$seleccionado = "";
		}
		$imprime = $imprime . "<option value='".$i."' ".$seleccionado.">".$i."</option>";
	}
	$imprime = $imprime . "</select>";
	
	$meses = array ("ENERO", "FEBRERO", "MARZO", "ABRIL", "MAYO", "JUNIO", "JULIO", "AGOSTO", "SEPTIEMBRE", "OCTUBRE", "NOVIEMBRE", "DICIEMBRE");	
	$imprime = $imprime . " Mes: <select name='".$mes5."' id='".$mes5."' title='Selecciona mes' style='background:".$color."'>";			
	$imprime = $imprime . "<option value=''>-MES-</option>";
	for($j=1;$j<=12;$j++){
		if($j == $mesDB){
			$seleccionado2 = " selected ";
		}else{
			$seleccionado2 = " ";
		}
		$imprime = $imprime . "<option value='".$j."' ".$seleccionado2." >".$meses[$j-1]."</option>";
	}									
	$imprime = $imprime . "</select>";
	
	$imprime = $imprime . " A&ntilde;o: <select name='".$ano5."' id='".$ano5."' title='Selecciona a&ntilde;o' style='background:".$color."'>";
	$imprime = $imprime . "<option value=''>-A&Ntilde;O-</option>";
	for($k=1920;$k<=2025;$k++){
	
		if($k == $anoDB){
				$seleccionado3 = " selected ";
		}else{
				$seleccionado3 = "";
		}
		$imprime = $imprime . "<option value='".$k."' ".$seleccionado3.">".$k."</option>";

	}                  
	$imprime = $imprime . "</select><br /><div style='font-size:10px'>NOTA: PRESENTAR ORIGINAL Y COPIA PARA INGRESAR <br /> AL EXPEDIENTE EL DOCUMENTO ACTUALIZADO.</div></td>"; 
		$imprime = $imprime . "</td>"; 
	
	return $imprime; 
}


function decim($cant){
	switch (strlen($cant)) {
		case 1:
			$cant = "&#36; ".$cant;
			break;
		case 2:
			$cant = "&#36; ".$cant;
			break;
		case 3:
			$cant = "&#36; ".$cant;
			break;
		case 4:
			$cant = "&#36; ".substr($cant, 0, 1).",".substr($cant, -3);
			break;
		case 5:
			$cant = "&#36; ".substr($cant, 0, 2).",".substr($cant, -3);
			break;
		case 6:
			$cant = "&#36; ".substr($cant, 0, 3).",".substr($cant, -3);
			break;
		case 7:
			$cant = "&#36; ".substr($cant, 0, 1).",".substr($cant, -6, -3).",".substr($cant, -3);
			break;
			
	}
	echo $cant;
}

    


?>