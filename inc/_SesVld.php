<?php
	session_start();
	
	if (!isset($_SESSION["SesIdSolicitante"])) {
		header("location: r3seccion1E.php");
	}
	
	if (!isset($_SESSION["SesIdSolicitante"]) && isset($_SESSION["SesRegConfirmado"])) {
		header("location: r3avisoTerminacion.php");
	}
	
	if (!isset($_SESSION["nivel"])) {
		header("location: r3seleccionescuelaE.php");
	}
?>