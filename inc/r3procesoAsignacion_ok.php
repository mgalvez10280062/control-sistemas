<?php
class Asignacion{

    
    
    function cancelarNoDocumentacion($db_pdo){
        	
        $qrySinDoc = "SELECT s.kcvesolicitante as kcve, s.odocumentacionentregada_s as doc
        FROM r3solicitantetutors s
        WHERE (s.odocumentacionentregada_s is null or s.odocumentacionentregada_s = 'N')
        AND s.obeneficiado_s is null
        AND s.ocancelado_s is null
        AND s.oejercicio='2012'
        AND (s.oposiblehermano_s is null or s.oposiblehermano_s <> 'R')"; //rojos pendientes
        $rslSinDoc = $db_pdo->query($qrySinDoc) or die("ERROR Query SInDoc... ".$qrySinDoc);
        
        $cancelados = 0;
                                
        while($rowSinDoc = $rslSinDoc->fetch(PDO::FETCH_ASSOC)){
 
            $upSinDoc = "UPDATE r3solicitantetutors 
            SET ocancelado_s = 'S', ";
            if($rowSinDoc["DOC"] == NULL){
                $upSinDoc = $upSinDoc . " omotivocancelacion_s = 4 ";
            }else{
                $upSinDoc = $upSinDoc . " omotivocancelacion_s = 11 ";
            }	
            $upSinDoc = $upSinDoc . " WHERE kcvesolicitante = ".$rowSinDoc["KCVE"]." 
            AND oejercicio='2012'";

            $cancelados = $cancelados + $db_pdo->exec($upSinDoc) or die("UPDATE Sindoc Error... ". $upSinDoc);
			
        }
                
        return $cancelados;
        
                        
    }
      
    
        
            
        
            
    
        
        
        
        
	
    function verificarPromedioPlaneacion($db_pdo){
       
        //PRIMERO SE EJECUTARA PARTE 1 Y DESPUES PARTE 2
        /*
        //1
        $qry = "SELECT s.kcvesolicitante as kcve, p.opromedio as pp, s.opromedio_s as ps, s.opromediocapturadoc_s as pd,
        s.onivel_s as nivel, s.ogrado_s as grado, s.overificacionplaneacion_s
        FROM r3solicitantetutors s, r3planeacion2012 p
        WHERE (s.ocurp_s = p.ocurp)
        AND s.oejercicio='2012'
        AND s.ofoliobeca_s is not null
        AND s.ocancelado_s is null";
            
        $rsl = $db_pdo->query($qry) or die ($qry);
	
        $cadena = "";        
        $promverificado = 0;
                    
        while($row = $rsl->fetch(PDO::FETCH_ASSOC)){
            
            $prom = $row["PP"];
            
            
            if(($row["NIVEL"] == "PES" || $row["NIVEL"] == "PST" || ($row["NIVEL"] == "PPR" && $row["GRADO"] > 1))){
            
                
               
                if($row["PD"]<>-1 && ($row["PD"]!=NULL || $row["PD"]!='')){
                    
                    //s.opromediocapturadoc_s<>-1.0 DIFERENTE DE NULL
                    //SE VA s.opromediocapturadoc_s
                    $q = "UPDATE r3solicitantetutors
                    SET opromedioverificado_s = ".$row["PD"]."
                    WHERE kcvesolicitante = " . $row["KCVE"]."
                    AND oejercicio='2012'";
                    $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 1... ". $q); 
                    
                }else{
                    if($row["PD"]==-1){
                        
                        //s.opromediocapturadoc_s==-1.0
                        //SE VA CON promedio de solicitud para que no se cancele
                        $q = "UPDATE r3solicitantetutors
                        SET opromedioverificado_s = ".$row["PS"]."
                        WHERE kcvesolicitante = " . $row["KCVE"]."
                        AND oejercicio='2012'";
                        
                        $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 2... ". $q); 
                            
                                
                        //s.opromediocapturadoc_s==-1.0
                        //SE VA CON 7.0
                        //$q = "UPDATE r3solicitantetutor23082012
                        //SET opromedioverificado_s = 7.0
                        //WHERE kcvesolicitante = " . $row["KCVE"]."
                        //AND oejercicio='2012'";
                        //$promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 2... ". $q); 
                        
                    }else{
                        
                        if(($row["PD"]==NULL || $row["PD"]=='') && $row["PP"]>0){
                            
                            //s.opromediocapturadoc_s ES NULL y promplan diferente a 0.0
                            //SE VA PROM DE PLANEACION
                            $q = "UPDATE r3solicitantetutors
                            SET opromedioverificado_s = ".$row["PP"]."
                            WHERE kcvesolicitante = " . $row["KCVE"]."
                            AND oejercicio='2012'";
                            $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 3... ". $q);  
                                    
                        }else{
                            
                            if(($row["PD"]==NULL || $row["PD"]=='') && $row["PP"]==0){
                                //s.opromediocapturadoc_s ES NULL y promplan es igual a 0.0
                                //se va promnedio solicitud 
                                $q = "UPDATE r3solicitantetutors
                                SET opromedioverificado_s = ".$row["PS"]."
                                WHERE kcvesolicitante = " . $row["KCVE"]."
                                AND oejercicio='2012'";
                                $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 4... ". $q);   
                                
                            }
                        }
                        
                    }
                }
                
                
                
                
            }else{
                
                
                //de pjn y ppr con grado 1 se va el promedio de planeacion no importa si es 0.0
                //despues se eliminara en otro proceso
                $q = "UPDATE r3solicitantetutors
                SET opromedioverificado_s = ".$prom."
                WHERE kcvesolicitante = " . $row["KCVE"]."
                AND oejercicio='2012'";
                $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 1... ". $q);
                
                
            }
            
            
            
                
                 
                
            
        }
        */
            
         
            
                
            
            
            
                                        
            
            
        //2
        $qry = "SELECT s.kcvesolicitante as kcve, s.opromedio_s as ps, s.opromediocapturadoc_s as pd, s.opromedioverificado_s as pv,
        s.onivel_s as nivel, s.ogrado_s as grado, s.overificacionplaneacion_s
        FROM r3solicitantetutors s
        WHERE s.oejercicio='2012'
        AND s.ofoliobeca_s is not null
        AND s.ocancelado_s is null
        AND s.opromedioverificado_s is null";
        
        $rsl = $db_pdo->query($qry) or die ($qry);
	
        $promverificado = 0;
                    
        while($row = $rsl->fetch(PDO::FETCH_ASSOC)){
            
     
		
            if(($row["NIVEL"] == "PES" || $row["NIVEL"] == "PST" || ($row["NIVEL"] == "PPR" && $row["GRADO"] > 1))){
            
            
                    
                
                if($row["PD"]<>-1 && ($row["PD"]!=NULL || $row["PD"]!='')){
                    
                    //s.opromediocapturadoc_s<>-1.0 DIFERENTE DE NULL
                    //SE VA s.opromediocapturadoc_s
                    $q = "UPDATE r3solicitantetutors
                    SET opromedioverificado_s = ".$row["PD"]."
                    WHERE kcvesolicitante = " . $row["KCVE"]."
                    AND oejercicio='2012'";
                    $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 1... ". $q); 
                    
                }else{
                    if($row["PD"]==-1){
                       
                        
                        //s.opromediocapturadoc_s==-1.0
                        //SE VA CON promedio de solicitud
                        $q = "UPDATE r3solicitantetutors
                        SET opromedioverificado_s = ".$row["PS"]."
                        WHERE kcvesolicitante = " . $row["KCVE"]."
                        AND oejercicio='2012'";
                        $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 2... ". $q); 
                                
                            
                        //s.opromediocapturadoc_s==-1.0
                        //SE VA CON 7.0
                        //$q = "UPDATE r3solicitantetutor
                        //SET opromedioverificado_s = 7.0
                        //WHERE kcvesolicitante = " . $row["KCVE"]."
                        //AND oejercicio='2012'";
                        //$promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 2... ". $q); 
                        
                    }else{
                        
                        if($row["PD"]==NULL || $row["PD"]==''){
                            
                            //s.opromediocapturadoc_s ES NULL 
                            //SE VA PROM SOL
                            $q = "UPDATE r3solicitantetutors
                            SET opromedioverificado_s = ".$row["PS"]."
                            WHERE kcvesolicitante = " . $row["KCVE"]."
                            AND oejercicio='2012'";
                            $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 3... ". $q);  
                                
                        }   
                        
                    }
                }
                    
                
            }else{
                
                    
                //de pjn y ppr con grado 1 se va el promedio 0.0
                //despues se eliminara en otro proceso
                $q = "UPDATE r3solicitantetutors
                SET opromedioverificado_s = 0
                WHERE kcvesolicitante = " . $row["KCVE"]."
                AND oejercicio='2012'";
                $promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error 1... ". $q);
                
                    
            }
            
            
           
                
                
                
                
                
                    
             
                
                
            
        }
        
        
            
            
       
                
        
            
                
                    
            
        
        
        
             
        //obtenemos el promedio de planeacion con un decimal y sin redondearlo
        //$multiplicador = pow (10,1);
        //$resultado = ((int)($row["PP"] * $multiplicador)) / $multiplicador;
        //$prom =  number_format($resultado, 1);
        //$prom = $row["PP"];
        
        //$q = "UPDATE r3solicitantetutor23082012
        //SET opromedioverificado_s = " .$prom."
        //WHERE kcvesolicitante = " . $row["KCVE"]."
        //AND oejercicio='2012'";

        //$promverificado = $promverificado + $db_pdo->exec($q) or die("UPDATE verifica promedio Error... ". $q); 

        
            
        
       
            
            
        
        return $promverificado;
        
        
    }
                
	
    
        
    
    
    
            
        
    
    
    
    
    
            
        
        
            
        
    
    function verificarPromedio($db_pdo){
            
            
        $PROMEDIO_MIN = 8.5;

        $sqlsol = "SELECT s.kcvesolicitante as kcve, s.ogrado_s as grado, s.opromedioverificado_s as promv, s.onivel_s as nivel 
        FROM r3solicitantetutors s
        WHERE s.obeneficiado_s is null
        AND s.ocancelado_s is null
        AND s.oejercicio='2012'
        AND s.ofoliobeca_s is not null
        AND (s.oposiblehermano_s is null or s.oposiblehermano_s <>'R')";
        
        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Promedio Verificado Error! " . $sqlsol);

        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){

            
            if(($rowsol["NIVEL"] == "PES" || $rowsol["NIVEL"] == "PST" || ($rowsol["NIVEL"] == "PPR" && $rowsol["GRADO"] > 1)) && ($rowsol["PROMV"] != NULL && trim($rowsol["PROMV"]) != "")){
					
                if($rowsol["PROMV"] < $PROMEDIO_MIN){
                            
                    $sqlup = "UPDATE r3solicitantetutors SET
                    ocancelado_s = 'S',
                    omotivocancelacion_s = 5
                    WHERE kcvesolicitante = ".$rowsol["KCVE"]."
                    AND oejercicio='2012'";
						
                    $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Promedio Verificado Error! " . $sqlup);
						
                }
                
            }
                    
				
        }
        return $i;
    }
    
        
        
            
    
        
    
    
            
    
    
        
    
    function marcarPosiblesHermanos($db_pdo){
		
        
        
        //concatenamos apellidopaterno y materno  APT
        //obtenemos cuales se repiten y los ordenamos por APT
        /*$sql = "SELECT apt FROM
        (
                SELECT concat(trim(s1.oapellidopaterno_s), trim(s1.oapellidomaterno_s)) apt
                FROM r3solicitantetutor23082012 s1
                WHERE s1.odocumentacionentregada_s = 'S'
                AND s1.obeneficiado_s is null
                AND s1.ocancelado_s is null
                AND s1.oejercicio='2012'
                AND (s1.oposiblehermano_s is null OR s1.oposiblehermano_s <> 'R')
        )
        group by apt
        having count(apt) > 1;";*/
			  
			
        //muestra los registros que estan repetidos todos los lopezlopez, todos los aguilarsanchez
        $qry = "SELECT s.kcvesolicitante as kcve, s.onombre_s, s.oapellidopaterno_s,
        s.oapellidomaterno_s, s.oejercicio
        FROM r3solicitantetutors s

        WHERE concat (trim(s.oapellidopaterno_s), trim(s.oapellidomaterno_s)) in
        (
                SELECT apt FROM
                (
                        SELECT 
                        concat(trim(s1.oapellidopaterno_s), trim(s1.oapellidomaterno_s)) apt
                        --concat(s1.oapellidopaterno_s, s1.oapellidomaterno_s) apt
                        FROM r3solicitantetutors s1
                        WHERE s1.odocumentacionentregada_s = 'S'
                        AND s1.obeneficiado_s is null
                        AND s1.ocancelado_s is null
                        AND s1.oejercicio='2012'
                        AND (s1.oposiblehermano_s is null OR s1.oposiblehermano_s <> 'R')
                )
                group by apt
                having count(apt) > 1
        )

        AND s.odocumentacionentregada_s='S'
        AND s.obeneficiado_s is null
        AND s.ocancelado_s is null
        AND (s.oposiblehermano_s is null OR s.oposiblehermano_s <>'R')
        AND s.oejercicio='2012'
        ORDER BY s.oapellidopaterno_s, s.oapellidomaterno_s, s.onivel_s, s.ogrado_s";

        $rslH = $db_pdo->query($qry) or die("Query Posibles Hermanos Error... ". $qry);

        $marcado =0;
        
        while($rowH = $rslH->fetch(PDO::FETCH_ASSOC)){
                
            //cancelado por hermano(3)
            $qryH = "UPDATE r3solicitantetutors 
            SET oposiblehermano_s = 'S'
            WHERE kcvesolicitante = ".$rowH["KCVE"]." 
            AND oejercicio='2012'";

            $marcado = $marcado + $db_pdo->exec($qryH) or die($qryH);
        }
        
        return $marcado;
        
            
    }   
            
    
    
    
        
            
        
    
        
        
        
    
        
            
    
    
                
        
    
    function cancelarHermanos($db_pdo){
            
                
        $sql = "SELECT s.kcvesolicitante as kcvesol, replace(replace(s.onombre_t, \". \", \"\"), \" \", \"\") as TNOM,
        replace(replace(s.oapellidopaterno_t, \". \", \"\"), \" \", \"\") as TAP,
        replace(replace(s.oapellidomaterno_t, \". \", \"\"), \" \", \"\") as TAM
        FROM r3solicitantetutors s
        WHERE replace(replace(concat(s.oapellidopaterno_s, concat(s.oapellidomaterno_s, concat(s.oapellidopaterno_t, concat(s.oapellidomaterno_t, s.onombre_t)))), \". \", \"\"), \" \", \"\")
        in
        (
                SELECT apt FROM
                (
                        SELECT replace(replace(concat(s1.oapellidopaterno_s, concat(s1.oapellidomaterno_s, concat(s1.oapellidopaterno_t, concat(s1.oapellidomaterno_t, s1.onombre_t)))), \". \", \"\"), \" \", \"\") apt
                        FROM r3solicitantetutors s1
                        WHERE s1.odocumentacionentregada_s='S'
                        and s1.obeneficiado_s is null
                        and s1.ocancelado_s is null
                        AND s1.oejercicio='2012'
                        and s1.oposiblehermano_s='S'
                )
                group by apt
                having count(apt) > 1
        )
        AND s.odocumentacionentregada_s='S'
        AND s.obeneficiado_s is null
        AND s.ocancelado_s is null
        AND s.oposiblehermano_s='S'
        AND s.oejercicio='2012'
        ORDER BY s.oapellidopaterno_s, s.oapellidomaterno_s, s.onivel_s DESC, s.ogrado_s DESC";
	
        $rslt = $db_pdo->query($sql) or die($sql);
        while($row = $rslt->fetch(PDO::FETCH_ASSOC)){
                   
                
            $qrycancel = "UPDATE r3solicitantetutors SET
            ocancelado_s = 'S',
            omotivocancelacion_s = 3
            WHERE kcvesolicitante = ".$row["KCVESOL"]."
            ANd oejercicio='2012'";
            $cancelados = $cancelados + $db_pdo->exec($qrycancel) or die($qrycancel);
                
	    	
        }
            
		     
        return $cancelados;
 
            
        
		
    }
    

    
    
    
    
            
        
            
        
        
        
    
    	
    function asignarPonderacionGrado($db_pdo){
        
            
            
        $sqlsol = "SELECT kcvesolicitante as kcve, ogrado_s as ogrado, onivel_s as onivel
        FROM r3solicitantetutors
        WHERE oejercicio='2012'
        AND ofoliobeca_s is not null
        AND ocancelado_s is null
        AND oponderaciongrado_s is null"; 

        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Grado Error! " . $sqlsol);

        $i = 0;
        
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
		
            
            $valor = 0;

            if($rowsol["OGRADO"] != NULL && trim($rowsol["OGRADO"]) != ""){
                $sqlgdo = "SELECT ovalor 
                FROM r3ctponderaciongrado
                WHERE onivel = '".$rowsol["ONIVEL"]."'
                AND ogrado = '" . $rowsol["OGRADO"] . "'
                AND oejercicio='2012'";
                $rsltgdo = $db_pdo->query($sqlgdo) or die("Query Select Valor Ponderacion Grado Error!");
                $rowgdo = $rsltgdo->fetch(PDO::FETCH_ASSOC);

                $valor = $rowgdo["OVALOR"];
            }

            $sqlup = "UPDATE r3solicitantetutors SET
            oponderaciongrado_s = ".$valor."
            WHERE kcvesolicitante = ".$rowsol["KCVE"]."
            AND oejercicio='2012'";
            


            $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Grado Error! " . $sqlup);
				
        }
        
        return $i;
		
    }


    
    
            
    
            
    
    
            
    
        
    
        
    
    function asignarPonderacionPromedio($db_pdo){
	
        

        $sqlsol = "SELECT kcvesolicitante as kcve, opromedioverificado_s as pv, onivel_s as onivel, ogrado_s as ogrado
        FROM r3solicitantetutors
        WHERE oejercicio='2012'
        AND ofoliobeca_s is not null
        AND ocancelado_s is null";  
        
        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Promedio Error! " . $sqlsol);
        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
                    
                    
            $valor = 0;

            if(($rowsol["ONIVEL"] == "PES" || $rowsol["ONIVEL"] == "PST" ||($rowsol["ONIVEL"] == "PPR" && $rowsol["OGRADO"] > 1))
            && ($rowsol["PV"] != NULL && $rowsol["PV"] > 0 && trim($rowsol["PV"]) != "")){

                $promedio = $rowsol["PV"] * 10;

                $sqlprom = "SELECT ovalor
                FROM r3ctponderacionpromedio
                WHERE opromedio = '" . $promedio . "'
                AND oejercicio='2012'";
                $rsltprom = $db_pdo->query($sqlprom) or die("Query Select Valor Ponderacion Promedio Error!");
                $rowprom = $rsltprom->fetch(PDO::FETCH_ASSOC);

                $valor = $rowprom["OVALOR"];
                
            }

            $sqlup = "UPDATE r3solicitantetutors SET
            oponderacionpromedio_s = ".$valor."
            WHERE kcvesolicitante = " . $rowsol["KCVE"]."
            AND oejercicio='2012'";

            $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Promedio Error! " . $sqlup."<br>".$sqlprom);
		
            
        }
		
        return $i;
		
    }
    

      
    
    
    
        
    
        
        
        
        
    
    
    function asignarPonderacionIngreso($db_pdo){
        //meter ingresos del captura doc        
        
        $sqlsol = "SELECT kcvesolicitante as kcve, onivel_s as onivel, opercepcionmensual_t as pm, opercepcionmensualdoc_t as pd
        FROM r3solicitantetutors
        WHERE oejercicio='2012'
        AND ofoliobeca_s is not null
        AND ocancelado_s is  null;";
	
                
        $rsltsol = $db_pdo->query($sqlsol) or die("Query Select Solicitante Ponderacion Ingreso Error! " . $sqlsol);
        $i = 0;
        while($rowsol = $rsltsol->fetch(PDO::FETCH_ASSOC)){
            
                
            $valor = 0;
            if($rowsol["PD"] != NULL && trim($rowsol["PD"]) != ""){
                
                $sqlingreso = "SELECT ovalor
                FROM r3ctponderacioningreso
                WHERE onivel = '".$rowsol["ONIVEL"]."'
                AND rcveingreso = '".$rowsol["PD"]."'
                AND oejercicio='2012'";
                $rsltingreso = $db_pdo->query($sqlingreso) or die("Query Select Valor Ponderacion Ingreso Error!");
                $rowingreso = $rsltingreso->fetch(PDO::FETCH_ASSOC);

                $valor = $rowingreso["OVALOR"];
            
            }else{
                            
                    
                $sqlingreso = "SELECT ovalor
                FROM r3ctponderacioningreso
                WHERE onivel = '".$rowsol["ONIVEL"]."'
                AND rcveingreso = '".$rowsol["PM"]."'
                AND oejercicio='2012'";
                $rsltingreso = $db_pdo->query($sqlingreso) or die("Query Select Valor Ponderacion Ingreso Error!");
                $rowingreso = $rsltingreso->fetch(PDO::FETCH_ASSOC);

                $valor = $rowingreso["OVALOR"];

            }
            
            
            
            
            $sqlup = "UPDATE r3solicitantetutors SET
            oponderacioningreso_s = ".$valor."
            WHERE kcvesolicitante = ".$rowsol["KCVE"]."
            AND oejercicio='2012'";
            
            $i = $i + $db_pdo->exec($sqlup) or die("Query Update Solicitante Ponderacion Ingreso Error! " . $sqlup);
                
            
            
        }
			
        return $i;
                
    }
    
        
    
        
    
    
            
            
    
            
    
    
        
    function asignarPonderacionTotal($db_pdo){
        
            
        
        $fecha = date('Y-m-d H:i:s');
        //NVL ("valor que puede tener null", "valor que tomaria en caso de tener null")
        
        
        $sql = "SELECT kcvesolicitante as kcve,
        NVL(oponderacioningreso_s, 0) ponderacioningreso,
        NVL(oponderacionpromedio_s, 0) ponderacionpromedio,
        NVL(oponderaciongrado_s, 0) ponderaciongrado
        FROM r3solicitantetutors 
        WHERE oejercicio='2012'
        AND ofoliobeca_s is not null
        AND ocancelado_s is  null"; 
  
        $rslt = $db_pdo->query($sql) or die("Query Select Solicitante Ponderacion Total Error! " . $sql);
        $i = 0;
        while ($row = $rslt->fetch(PDO::FETCH_ASSOC)) {
            
                            
            
            $ponderaciontotal = $row["PONDERACIONINGRESO"] + $row["PONDERACIONPROMEDIO"] + $row["PONDERACIONGRADO"];

            $sqlpond = "UPDATE r3solicitantetutors SET
            oponderaciontotalinicial_s = '".$ponderaciontotal."',
            oponderaciontotalasignada_s = '".$ponderaciontotal."',
            istatus = 'M',
            ifecmod = '".$fecha."'
            WHERE kcvesolicitante = '".$row["KCVE"]."'
            ANd oejercicio='2012'";

            $i = $i + $db_pdo->exec($sqlpond) or die("Query Update Solicitante Ponderacion Total Error! " . $sqlpond);
		
            
        }
        
        return $i; 
        
    }
    
    
    
        
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
        
    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
        
        
    
    
    function calcular5porciento($db_pdo){
        
        /*
        $cctnopermiconsol = array();
        $j = 0;
        
        //seleccionamos los cct que se repartieron en las sedes
        $sql ="select rcvecct from r3centrotrabajosede
        where oejercicio='2012'";
	
        $rsltot = $db_pdo->query($sql) or die("Query Select totalCTSEDE Error!");
        while ($row = $rsltot->fetch(PDO::FETCH_ASSOC)) {
        
            //buscamos si ese cct esta en las ccts permitidas
            $sql1 ="SELECT count(*) as CONT 
            FROM x1centrotrabajo c
            WHERE (c.ostatus = 1 OR c.ostatus = 4)
            AND (substr(c.oclave, 3,3)='PJN' or substr(c.oclave, 3,3)='PPR' or substr(c.oclave, 3,3)='PES' or substr(c.oclave, 3,3)='PST')
            AND c.oclave NOT IN (SELECT clave FROM r3cctsconrevocacion)
            AND c.rcvelocalidad_inegi is not null
            AND c.kcvect='".$row["RCVECCT"]."'";
		
            $rsltot1 = $db_pdo->query($sql1) or die("Query Select totalCT Error! 1".$sql1);
            $row1 = $rsltot1->fetch(PDO::FETCH_ASSOC);
                
            if($row1["CONT"]==1){
                //cct si esta en las permitida
            }else{
                
                //cct no esta en las permitidas
                //buscamos cuantas solicitudes tuvo ese cct
                $sql2 ="select count(*) as cont from r3solicitantetutor
                where ofoliobeca_s is not null
                and oejercicio='2012'
                and rcvecctactual_s='".$row["RCVECCT"]."'";

                $rsltot2 = $db_pdo->query($sql2) or die("Query Select totalCT Error! 2".$sql2);
                $row2 = $rsltot2->fetch(PDO::FETCH_ASSOC);
                
                if($row2["CONT"]>=1){
                    //este cct si tuvo solicitudes de beca
                    //se agrega a vector de cctnopermitidos con solicitudes
                    $cctnopermiconsol[$j] = $row["RCVECCT"];
                    $j++;   
                }   
                
                
                
            }
            
        }
          
        
        
        
        
        //para obtener los cct para agregar en capacidadbeca se seleccionaran 
        //los cct permitidos mas la cadena de cctnopermitidos con solicitud
        $sqlcctpermitidos1 = "SELECT c.kcvect as kcvect, c.oclave as cct, c.rcveturno as kcveturno
        FROM x1centrotrabajo c
        WHERE (c.ostatus = 1 OR c.ostatus = 4)
        AND (substr(c.oclave, 3,3)='PJN' or substr(c.oclave, 3,3)='PPR' or substr(c.oclave, 3,3)='PES' 
        or substr(c.oclave, 3,3)='PST')
        AND c.oclave NOT IN (SELECT clave FROM r3cctsconrevocacion)
        AND c.rcvelocalidad_inegi is not null"; 
        
        
        if(count($cctnopermiconsol)>0){
            
            //si hay cct no permitidos con solicitudes de becas
            $cad = "";  
            for($k=0; $k<=(count($cctnopermiconsol)-1); $k++){	
                if($k == (count($cctnopermiconsol)-1)){
                    $cad = $cad .$cctnopermiconsol[$k];
                }else{
                    $cad = $cad .$cctnopermiconsol[$k].", ";
                }
            }       
            $sqlcctpermitidos2 = " OR c.kcvect in (".$cad.");";
            
            
        }else{  
            $sqlcctpermitidos2 = "";
        }
        
            
                
        $sqlcctpermitidos = $sqlcctpermitidos1 . $sqlcctpermitidos2;
       
        
            
        
        
            
        
        
                
         
                
        
        
        
		
       
        $PORCIENTO = 0.05;
	
		
        $rsltcctpermitidos = $db_pdo->query($sqlcctpermitidos) or die("Query Select totalCT permitidos Error!");
        $i = 0;
        while ($rowcctpermitidos = $rsltcctpermitidos->fetch(PDO::FETCH_ASSOC)) {
	
                
                
            $i = $i + 1;  
                                    
            $sqlb = "select count(*) as total FROM r3planeacion2012
            WHERE occt='".$rowcctpermitidos["CCT"]."'";
            $rsltb = $db_pdo->query($sqlb) or die ("Query error b...");
            $rowb = $rsltb->fetch(PDO::FETCH_ASSOC);
                    
				
            $porcentajeAlumnos = $rowb["TOTAL"] * $PORCIENTO;
            $totalPtos = $porcentajeAlumnos * 100;
                        
                
            $sqlin = "INSERT INTO r3capacidadbecas (rcvecct, rcveturno, ototalalumnos, ototalbecascompletas, 
            ototalpuntos, opuntosasignados, opuntosrestantes, oejercicio) 
            VALUES (" . $rowcctpermitidos["KCVECT"] . ",". $rowcctpermitidos["KCVETURNO"] . "," . $rowb["TOTAL"] . "," . $porcentajeAlumnos . ",
            " . $totalPtos . ",0," . $totalPtos .", '2012')";
				
            $db_pdo->query($sqlin) or die("Query Insert r3Capacidad  Error! <br/>".$sqlin);
                
                    
                    
        }  
        
        return $i;
	*/
                
        return 'NO NO NO';       
            
    }   
     
    
        
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
        
    
    
    
            
    
        
        
        
    
    
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
            
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   
    
    
    
    
    
        
    
    
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
        
    
        
    
    
                    
        
                
            
    
    
    function asignar1($db_pdo, $porcentaje, $ronda){
        
                
        $PORCENTAJE_INI = 25;
        $PORCENTAJE_MIN = 10;
                    
            
        $sqlct = "";  
        switch($ronda){ 
            case 1: /*$sqlct = "SELECT cap.kcvecapacidad, cap.rcvecct  as rcvecct, x.oclave, cap.rcveturno as rcveturno,
                    cap.ototalbecascompletas as ototalbecascompletas, cap.ototalpuntos as ototalpuntos,
                    cap.opuntosasignados as opuntosasignados, cap.opuntosrestantes as opuntosrestantes,
                    x.oclave as oclave, x.ofolioincorporacion as ofolioincorporacion, t.odescripcion as turno
                    FROM r3capacidadbecass cap, x1centrotrabajo x, x1ctturno t
                    WHERE (cap.rcvecct = x.kcvect)
                    AND (cap.rcveturno = t.kcveturno)
                    AND cap.oejercicio='2012'
                    AND cap.opuntosrestantes>='".$PORCENTAJE_INI."'
                    
                    AND (cap.kcvecapacidad>='15257')

                    order by cap.rcvecct";*/
                    //cct de r3capacidadbecas que tienen totaldealumnos mayor o igual a 5
                    
                
                
                
                    $sqlct = "SELECT cap.kcvecapacidad, cap.rcvecct  as rcvecct, x.oclave, cap.rcveturno as rcveturno,
                    cap.ototalbecascompletas_b as ototalbecascompletas, cap.ototalpuntos_b as ototalpuntos,
                    cap.opuntosasignados_b as opuntosasignados, cap.opuntosrestantes_b as opuntosrestantes,
                    x.oclave as oclave, x.ofolioincorporacion as ofolioincorporacion, t.odescripcion as turno
                    FROM r3capacidadbecas cap, x1centrotrabajo x, x1ctturno t
                    WHERE (cap.rcvecct = x.kcvect)
                    AND (cap.rcveturno = t.kcveturno)
                    AND cap.oejercicio='2012'
                    AND cap.opuntosrestantes_b>='".$PORCENTAJE_INI."'
                    AND cap.otipo_b='S3'
                    
                    
                    AND (cap.kcvecapacidad>='15085' AND cap.kcvecapacidad<='15288')
                    
                    order by cap.kcvecapacidad";
                
                
                                    
                                
                
                
                
            break;             
				                          
            case 2: $sqlct = "SELECT cap.kcvecapacidad, cap.rcvecct as rcvecct, x.oclave, cap.rcveturno as rcveturno,
                    cap.ototalbecascompletas as ototalbecascompletas, cap.ototalpuntos as ototalpuntos,
                    cap.opuntosasignados as opuntosasignados, cap.opuntosrestantes as opuntosrestantes, cap.opuntosguardados as guard,
                    x.oclave as oclave, x.ofolioincorporacion as ofolioincorporacion, t.odescripcion as turno
                    FROM r3capacidadbecas31082012 cap, x1centrotrabajo x, x1ctturno t
                    WHERE (cap.rcvecct = x.kcvect)
                    AND (cap.rcveturno = t.kcveturno)
                    AND cap.oejercicio='2012'
                    AND cap.ototalalumnos>=5			   
                    
                    AND cap.kcvecapacidad in (1,2,3)
                        
                        
                    order by cap.rcvecct"; 
                
                    
                    //AND (cap.kcvecapacidad>=13529 AND cap.kcvecapacidad<=13606)
                    //AND cap.opuntosrestantes>=".$PORCENTAJE_INI."
                    //ccts de r3capacidadcita que tienen totaldealumnos mayor o igual a 5 y que tienen mas de 10 puntosrestantes       
                    
            break;
        }
            
        
                    
            
        
        $rsltct = $db_pdo->query($sqlct) or die("Query Select1 Asignar Error! <br/>".$sqlct);

        $i = 0;
        $numreg = 0;
        $cadenasc = "";
        
        while ($rowct = $rsltct->fetch(PDO::FETCH_ASSOC)) {
                    
            $nivel = substr($rowct["OCLAVE"], 2, 3);
            $isAG = substr($rowct["OFOLIOINCORPORACION"], 0,2);
                
            $numreg = $numreg + 1;
                    
            $puntos = 0;
            $numerobecas = 0;
            $qryPAsignados = "";
                
                    
            
            
            
                    
            
            
            switch($ronda){
                case 1: 
                        
                        $puntos = $rowct["OTOTALPUNTOS"]; //total de puntos por cada cct
                        $ptosrestantes=$rowct["OPUNTOSRESTANTES"]; //puntos restantes
                        //$ptosguardados=150; //puntos guardados
                        
                            
                        
                        
                        /*

                        if(($ptosrestantes - $ptosguardados)>=0){
                            $realptosguardados=$ptosguardados;
                            $porcentajebecas=$ptosrestantes - $ptosguardados;
                            $puntos = $porcentajebecas;
                        }
                        if(($ptosrestantes - $ptosguardados)<0){
                            $realptosguardados=$ptosrestantes;
                            $porcentajebecas=0;
                            $puntos = $porcentajebecas;
                        }   
                        
                            
                            
                            
                          
                                                
                        $sqlptosguard = "UPDATE r3capacidadbecas23082012
                        SET opuntosguardados=".$realptosguardados."
                        WHERE rcvecct='".$rowct["RCVECCT"]."'

                        AND rcveturno='".$rowct["RCVETURNO"]."'
                        
                        AND oejercicio='2012'";
                        $rsltptosguard = $db_pdo->query($sqlptosguard) or die("Query ptosguard! <br/>".$sqlptosguard);
                        $rowptosguard = $rsltptosguard->fetch(PDO::FETCH_ASSOC);
                            
                         
						
                        if($realptosguardados<150){

                            $sqlguard1 = "UPDATE r3capacidadbecas23082012
                            SET opuntosrestantes=0
                            WHERE rcvecct='".$rowct["RCVECCT"]."'
                                    
                            AND rcveturno='".$rowct["RCVETURNO"]."'

                            AND oejercicio='2012'";
                            $rsltguard1 = $db_pdo->query($sqlguard1) or die("Query guard1! <br/>".$sqlguard1);
                            $rowguard1 = $rsltguard1->fetch(PDO::FETCH_ASSOC);

                        }
                        if($realptosguardados>=150){

                            $novo = ($rowct["OTOTALPUNTOS"])-($rowct["OPUNTOSASIGNADOS"] + $realptosguardados);

                            $sqlguard2 = "UPDATE r3capacidadbecas23082012
                            SET opuntosrestantes=".$novo."
                            WHERE rcvecct='".$rowct["RCVECCT"]."'
                                    
                            AND rcveturno='".$rowct["RCVETURNO"]."'

                            AND oejercicio='2012'";
                            $rsltguard2 = $db_pdo->query($sqlguard2) or die("Query guard2! <br/>".$sqlguard2);
                            $rowguard2 = $rsltguard2->fetch(PDO::FETCH_ASSOC);

                        } 
                        
			*/
                        
                        
                            
                            
                        
                        
			$porcentajebecas=$ptosrestantes;
                        $puntos = $porcentajebecas;			
		    
			/*
						
                        $porcentajebecas = ($porcentaje / 100) * $puntos; //sacamos el 80% de ese total de puntos
                            
                        
                        
                        if($rowct["RCVECCT"]=='9013'){
                            
                            //hay 2 en capbec
                            //verificar si ya se han repartido puntos de ese X1 %
                            //en los casos de tramite directo es cuando ya se han ocupado algunos puntos
                            $qryPAsignados = "SELECT NVL(SUM(oporcentajeasignado_s), 0) as suma
                            FROM r3solicitantetutor31082012
                            WHERE rcvecctactual_s = '".$rowct["RCVECCT"]."'
                                    
                            AND rcveturno_s ='".$rowct["RCVETURNO"]."'   
                                
                            AND oejercicio='2012'
                            AND obeneficiado_s = 'S'";
                            
                        }else{
                            
                            //verificar si ya se han repartido puntos de ese X1 %
                            //en los casos de tramite directo es cuando ya se han ocupado algunos puntos
                            $qryPAsignados = "SELECT NVL(SUM(oporcentajeasignado_s), 0) as suma
                            FROM r3solicitantetutor31082012
                            WHERE rcvecctactual_s = '".$rowct["RCVECCT"]."'
                            AND oejercicio='2012'
                            AND obeneficiado_s = 'S'";
                                
                        }   
                            
                            
                                    
                        $rsltAsig = $db_pdo->query($qryPAsignados) or die ("Query Puntos AsignadosError!!.... ". $qryPAsignados);
                        $rowptosAsignados = $rsltAsig->fetch(PDO::FETCH_ASSOC);
                        
                        if($rowptosAsignados["SUMA"] > 0){
                            $porcentajebecas = $porcentajebecas - $rowptosAsignados["SUMA"]; 
                            //sera los que sobro despues de tramites derectos
                            $puntos = $porcentajebecas;
							
                        }
                        */    
                            
                            
                            
                break;
                case 2: 
                        
                        
                    
                        if($rowct["RCVECCT"]=='9013'){
                            
                            //hay 2 en capbec
                                
                            //SUMAMOS EL PORCENTAJES DE QUIENES TIENE MOTIVO DE CANCELACION 2 O 6 los que no estan inscritos
                            $sqlsum = "SELECT SUM(NVL(OPORCENTAJEASIGNADO_S, 0)) AS SUMA FROM r3solicitantetutor31082012
                            WHERE RCVECCTACTUAL_S='".$rowct["RCVECCT"]."'
                            AND RCVETURNO_S='".$rowct["RCVETURNO"]."' 
                            AND OEJERCICIO='2012'
                            AND OCANCELADO_S='S'
                            AND (OMOTIVOCANCELACION_S='2' OR OMOTIVOCANCELACION_S='6')";
                                
                            
                            
                            
                        }else{
                            
                            
                            //SUMAMOS EL PORCENTAJES DE QUIENES TIENE MOTIVO DE CANCELACION 2 O 6 los que no estan inscritos
                            $sqlsum = "SELECT SUM(NVL(OPORCENTAJEASIGNADO_S, 0)) AS SUMA FROM r3solicitantetutor31082012
                            WHERE RCVECCTACTUAL_S='".$rowct["RCVECCT"]."'
                            AND OEJERCICIO='2012'
                            AND OCANCELADO_S='S'
                            AND (OMOTIVOCANCELACION_S='2' OR OMOTIVOCANCELACION_S='6')";

                            
                        }
                        
                    
                        
                        $rsltsum = $db_pdo->query($sqlsum) or die ("Query suma 2 o 6!!.... ". $sqlsum);
                        $rowsum = $rsltsum->fetch(PDO::FETCH_ASSOC);
						
						
                        $nvorest = $rowct["OPUNTOSRESTANTES"] + $rowct["GUARD"] + $rowsum["SUMA"];
                        $nvoasig = $rowct["OPUNTOSASIGNADOS"] - $rowsum["SUMA"];
						
                            
                        
						
                        $sqlupcap = "UPDATE r3capacidadbecas31082012
                        SET OPUNTOSASIGNADOS=".$nvoasig.",
                        OPUNTOSRESTANTES=".$nvorest.",
                        OPUNTOSGUARDADOS=0
                        WHERE RCVECCT='".$rowct["RCVECCT"]."'            
                        AND RCVETURNO='".$rowct["RCVETURNO"]."'
                        AND OEJERCICIO='2012'";
                        $rsltupcap = $db_pdo->query($sqlupcap) or die ("Query actualiza nvorest!!.... ". $sqlupcap);
                        $rowupcap = $rsltupcap->fetch(PDO::FETCH_ASSOC);
			
                        
                            
                        
                            
                            
                         if($rowct["RCVECCT"]=='9013'){
                            
                            //hay 2 en capbec
                            $sqlupsol = "UPDATE r3solicitantetutor31082012 SET
                            OPORCENTAJEASIGNADO_S=NULL
                            WHERE RCVECCTACTUAL_S='".$rowct["RCVECCT"]."'
                            AND RCVETURNO_S='".$rowct["RCVETURNO"]."' 
                            AND OEJERCICIO='2012'
                            AND OCANCELADO_S='S'
                            AND (OMOTIVOCANCELACION_S='2' OR OMOTIVOCANCELACION_S='6')";
                                
                                
                        }else{
                            
                            $sqlupsol = "UPDATE r3solicitantetutor31082012 SET
                            OPORCENTAJEASIGNADO_S=NULL
                            WHERE RCVECCTACTUAL_S='".$rowct["RCVECCT"]."'
                            AND OEJERCICIO='2012'
                            AND OCANCELADO_S='S'
                            AND (OMOTIVOCANCELACION_S='2' OR OMOTIVOCANCELACION_S='6')";
                            
                        }
                        
                            
                                
                            
                        
                        
                        $rsltupsol = $db_pdo->query($sqlupsol) or die ("Query actualiza upsol!!.... ". $sqlupsol);
                        $rowupsol = $rsltupsol->fetch(PDO::FETCH_ASSOC);
		
                        
					
                            
						
						 
						
                        $puntos = $nvorest; //total de puntos son los puntosrestantes
                        $porcentajebecas = $puntos; //porcentaje para becas es lo mismo de los puntos restantes
						
						
                        
                break;
            }
            
            
                  
                
            
                
                    
            
            
            

            if($porcentajebecas >= $PORCENTAJE_MIN){ // si trae >= 10 puntos
                                    
                
                if($porcentajebecas > $PORCENTAJE_INI){ //si el porcentaje a asignar es mayor a 25
                                    
                    $residuo = $porcentajebecas % $PORCENTAJE_INI; //residuo es lo que sobra de dar todas las becas al 25%
                    $porcentajebecas = $porcentajebecas - $residuo; //porcentajea asignar es la cantidad exacta de becas al 25%
                        
                }else{
                    
                    if($puntos >= $PORCENTAJE_INI){
                        $residuo = $puntos % $PORCENTAJE_INI;//lo que sobra de dar los puntos a becas del 25%
                        $porcentajebecas = $puntos - $residuo;//lo que se asignara en becas exactas al 25
                    }else{
                        $residuo = $puntos;
                    }	                    
                        
                }
                    
                
                        
                
                

                $numerobecas = floor($porcentajebecas / $PORCENTAJE_INI); //divide la cantidadde becas entre 25
                                    
		
                    
                if($residuo >= $PORCENTAJE_MIN && $ronda == 2){ //si es la 2da. ronda (20%) y sobran 10 o mas
                    $numerobecas = $numerobecas + 1;
                }
				
                if($numerobecas >= 1){
                        
                    $ben = $this->setCandidatos($rowct["RCVECCT"],$rowct["RCVETURNO"], $nivel, $isAG, $numerobecas, $db_pdo);
                    $i = $i + $ben; 
                    
                    
                    
                }
                    
                        
                    
            }
                    
                
                    
            //echo "<br/>Residuo: " . $residuo . "<br/>";
            //echo "<br/>" . $rowct['OTOTALPUNTOS'] . "<br/>";
            //echo "<br/>" . $porcentajebecas . "<br/>";
            //echo "Puntos asignados:" . $rowptosAsignados['SUMA'] . "<br/>";
            //echo "Numero de Becas: " . $numerobecas . "<br/><br/>";
            echo "reg: " . $numreg . " kvecct : " . $rowct["RCVECCT"] . " turno: " . $rowct["RCVETURNO"] . " beneficiados: " . $ben."<br/>";
                
            
                        
        }       
        
        return $i;
            
		
    } 
    
    
    
    
        
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
    
    function setCandidatos($kcct, $turno, $nivel, $isAG, $numerobecas, $db_pdo){
            

        $PORCENTAJE_INI = 25;

        $gradoini = 1;
        $grados = 0;
		
            
		
        if($nivel == "PJN" || $nivel == "PES" || $nivel == "PST"){
            $grados = 3;
            $gradosparticipantes = 3;
            //"En planteles con Acuerdo "AG", s�lo se beneficia a los alumnos
            //de segundo y tercer grado, ya que el primer grado no es obligatorio"
            if($nivel == "PJN" && $isAG == "AG"){
                $gradoini = 2;
                $gradosparticipantes = 2;
            }
        }else{
            $grados = 6;
            $gradosparticipantes = 6;
        }
	
            
            
        //PES PST y PJN
        //$gradoini=1;
        //$grados=3;
        //$gradosparticipantes=3;
        //PPR
        //$gradoini=1;
        //$grados=6;
        //$gradosparticipantes=6;
        //PJN is AG
        //$gradoini=2;
        //$grados=3;
        //$gradosparticipantes=2;
            
	
          
                
        
        $gradocantidad = $this->distribuir($numerobecas, $gradosparticipantes, $gradoini, $grados, $kcct, $turno, $db_pdo);
            
        
        //$this->printA($gradocantidad);//muestra el contenido del vector gradocantidad
            
        
        $sql = "";
		
        for($i=$gradoini; $i<=$grados; $i++){ //empieza recorrido 123456 dependiendo nivel si es AG 23
            
            if($gradocantidad[$i]["cantidad"] > 0){ // si al menos una beca por grado....
				
                if($i > 1 && !trim($sql) == ""){ //entra en la segunda posicion
                    $sql = $sql . " UNION";
                }
                
                $sbqry = $this->queryCandidatosXcentroTrabajoGrado($gradocantidad[$i]["cantidad"], $kcct, $turno, $i);
                
                    
                    
                $sql = $sql . " SELECT a.kcvesolicitante as kcvesolicitante, a.ogrado_s as grado, a.oprioridad_s as prioridad, 
                a.oponderaciontotalasignada_s as ponderaciontotasig, a.opromedioverificado_s as promver
                FROM r3solicitantetutor a, (". $sbqry .") b
                WHERE a.kcvesolicitante = b.kcvesolicitante";
                   
            }
        }   
            
            
                    
            
		
        if(!trim($sql) == ""){ 
            $sql = $sql . " ORDER BY grado ASC, promver DESC, ponderaciontotasig DESC, prioridad DESC";
        } 
	
            
            
		
        //echo $sql;//consulta kcve grado, prioridad, ponderaciontotalasignada y promedioverificado de los grados participantes
        //pero solo los que se tienen en [cantidad] dentro del vector
	
	
                
            
        
        $rslt = $db_pdo->query($sql);// or die("Query Select Union Error! <br/>hh" . $sql);

        //MARCAR A TODOS LOS CANDIDATOS
        $i = 0;
        if($rslt != NULL){
            while ($row = $rslt->fetch(PDO::FETCH_ASSOC)) {
                
                $i = $i + 1;
                    
                $sqlcandi = "UPDATE r3solicitantetutor SET
                obeneficiado_s = 'S',
                oposiblehermano_s = 'X'
                WHERE kcvesolicitante = '".$row["KCVESOLICITANTE"]."'";

                $db_pdo->query($sqlcandi) or die("Query Update Solicitante Candidato Error! " . $sqlcandi);
                    
            	$this->asignarPorcentaje($db_pdo, $row["KCVESOLICITANTE"], $PORCENTAJE_INI, $kcct, $turno);
                        
            }
      	}
		
        return $i;
            
    }
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
        
    
    
    
        
    
    
    
    function distribuir($numerobecas, $gradosparticipan, $gradoini, $gradostot, $kcct, $turno, $db_pdo){
            
                
	
        $cantidad = floor($numerobecas / $gradosparticipan); 
        $restantes = $numerobecas % $gradosparticipan;
        $b = 0;
        $cc = 0;
                
        
        //$cad = "";
                
            
        for($k = $gradoini; $k<=$gradostot; $k++){
            
             
            $gradocantidad[$k]["grado"] = $k;
            $gradocantidad[$k]["cantidad"] = $cantidad;
	
                
                 
            $conteo = $this->queryContarCandidatosXGrado($kcct, $turno, $k, $db_pdo); //obtenemos el numero de cantidatos xgrado
            $gradocantidad[$k]["conteo"] = $conteo;
                
                    
	    
            if($conteo <= $cantidad){ // si los cantidatos son menores o iguales  ala cantidad de becas  xgrado
                $gradocantidad[$k]["status"] = 0;
            }else{
                $gradocantidad[$k]["status"] = 1;//loscandidatos son mayores a la cantidad de becas xgrado
                $cc = $cc + 1;
            }
	
            //$cad = $cad.$gradocantidad[$k]["grado"]."  ".$gradocantidad[$k]["cantidad"]."  ".$gradocantidad[$k]["conteo"]."  ".
            //$gradocantidad[$k]["status"]."<br>";
			
        }
                
            
        
        
        
		
		
        for($i = $gradostot; $i >= $gradoini; $i--){ //decrementa  654321
		
            
	    	
            if($gradocantidad[$i]["cantidad"] > $gradocantidad[$i]["conteo"]){ //si la cantidad de beca es mayor a los candidatos
                        
			
                $b = 1;
                $cantidad = $gradocantidad[$i]["cantidad"];
                $gradocantidad[$i]["cantidad"] = $gradocantidad[$i]["conteo"];
                
                $faltantes = $cantidad - $gradocantidad[$i]["conteo"];
                $nvacantidad = $faltantes + $restantes;
                $restantes = 0;
				
                for ($h = $gradostot; $h >= $gradoini; $h--){//decrementa 654321
					
                    if($gradocantidad[$h]["status"] == 1 && $nvacantidad > 0){
                        $gradocantidad[$h]["cantidad"] = $gradocantidad[$h]["cantidad"] + 1;
                        $nvacantidad = $nvacantidad - 1;

                        if($gradocantidad[$h]["conteo"] == $gradocantidad[$h]["cantidad"]){
                            $gradocantidad[$h]["status"] = 0;
                            $cc = $cc - 1;
                        }
                    }
                    if($nvacantidad > 0 && $h == $gradoini && $cc > 0) $h = $gradostot + 1;
                    //hace que el for inicie de nuevo la regresion
                            
                }
				
				
				
				
            }
             
			
        }
	
        
        
            
        
        
        
		
		
        if($restantes > 0 && $b == 0){
            $nvacantidad = $restantes;
            for ($h = $gradostot; $h >= $gradoini; $h--) {//empieza dremento 654321
                if($gradocantidad[$h]["status"] == 1 && $nvacantidad > 0){
                    $gradocantidad[$h]["cantidad"] = $gradocantidad[$h]["cantidad"] + 1;
                    $nvacantidad = $nvacantidad - 1;
                    if($gradocantidad[$h]["conteo"] == $gradocantidad[$h]["cantidad"]){
                        $gradocantidad[$h]["status"] == 0;
                        $cc = $cc - 1;
                    }
                }
                if($nvacantidad > 0 && $h == $gradoini && $cc > 0) $h = $gradostot + 1;
            }
        }
		
	
        
            
                
        
        
        //for($s = $gradoini; $s<=$gradostot; $s++){
            //$cad = $cad.$gradocantidad[$s]["grado"]."  ".$gradocantidad[$s]["cantidad"]."  ".$gradocantidad[$s]["conteo"]."  ".
            //$gradocantidad[$s]["status"]."<br>";
        //}
        //return $cad;
        
        
        return $gradocantidad;
		
    }
    
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    
    
    
    
    
        
        
    
    
    
    
    
    function queryCandidatosXcentroTrabajoGrado($numerobecas, $kcct, $turno, $grado){
        
        //obtiene query 
        //ordena los candidatos para becas
        //ordenandolos iniciando con los promedios mas altos, 
        //despues las ponderacionestotales asignadas mas altas
        //y las prioridades ordenadas por las mas altas
        //r3solicitantetutor23082012
        //r3capacidadbecas14082012   
        //r3controlstatus23082012  
        
         
                           
                            
        
        if($kcct=='9013'){
                            
            //hay 2 en capbec
            $sbqry = "SELECT FIRST ".$numerobecas." s.kcvesolicitante
            FROM r3solicitantetutor s, r3controlstatus c
            WHERE (s.kcvesolicitante = c.rcvesolicitante)
            AND s.rcvecctactual_s = '".$kcct."'
            AND s.rcveturno_s = '".$turno."'
            AND c.ofechaentrega IS NOT NULL
            AND s.ocancelado_s IS NULL
            AND s.odocumentacionentregada_s='S'
            AND s.obeneficiado_s IS NULL
            AND s.oejercicio='2012'
            AND s.ogrado_s = '".$grado."'
            ORDER BY opromedioverificado_s DESC, oponderaciontotalasignada_s DESC, oprioridad_s DESC";
                
                
        }else{
            
            
            $sbqry = "SELECT FIRST ".$numerobecas." s.kcvesolicitante
            FROM r3solicitantetutor s, r3controlstatus c
            WHERE (s.kcvesolicitante = c.rcvesolicitante)
            AND s.rcvecctactual_s = '".$kcct."'
            AND c.ofechaentrega IS NOT NULL
            AND s.ocancelado_s IS NULL
            AND s.odocumentacionentregada_s='S'
            AND s.obeneficiado_s IS NULL
            AND s.oejercicio='2012'
            AND s.ogrado_s = '".$grado."'
            ORDER BY opromedioverificado_s DESC, oponderaciontotalasignada_s DESC, oprioridad_s DESC";

        }
        
        return $sbqry;
            
    }   
    
    
    
        
    
    
        
    
    
    
    
    
    
        
    
    
    
    
    
    
        
        
    
    function queryContarCandidatosXGrado($kcct, $turno, $grado, $db_pdo){
        
        //cuenta los candidatos que tenemos en cierto cct  de acuerdo a un grado en especifico
        //los candidatos deben tener registro completo
        //-no deben estar cancelados
        //-no deben ser posible hermano
        //-deben tener la documentacion entregada
        //-no deben ser beneficiados
        //r3solicitantetutor23082012
        //r3capacidadbecas14082012   
        //r3controlstatus23082012    
                
            
        if($kcct=='9013'){
                            
            //hay 2 en capacidadbecas
            $sbqry = "SELECT count(s.kcvesolicitante)
            FROM r3solicitantetutor s, r3controlstatus c
            WHERE (s.kcvesolicitante = c.rcvesolicitante)   
            AND s.rcvecctactual_s = '".$kcct."'
            AND s.rcveturno_s = '".$turno."'
            AND c.ofechaentrega IS NOT NULL
            AND s.ocancelado_s IS NULL
            AND s.odocumentacionentregada_s='S'
            AND s.obeneficiado_s IS NULL
            AND s.oejercicio='2012'
            AND s.ogrado_s = '".$grado."'"; 
                    
                
        }else{
            
            $sbqry = "SELECT count(s.kcvesolicitante)
            FROM r3solicitantetutor s, r3controlstatus c
            WHERE (s.kcvesolicitante = c.rcvesolicitante)   
            AND s.rcvecctactual_s = '".$kcct."'
            AND c.ofechaentrega IS NOT NULL
            AND s.ocancelado_s IS NULL
            AND s.odocumentacionentregada_s='S'
            AND s.obeneficiado_s IS NULL
            AND s.oejercicio='2012'
            AND s.ogrado_s = '".$grado."'"; 
            
        }
        
        
            
        
        

        $rslt1 = $db_pdo->query($sbqry) or die ("Query distribuir Gradio Error!!.... ". $sbqry);
        $num = $rslt1->fetchColumn();

        return $num;
    }
    
    
    
    
    
            
    
    
        
    
    
    
    
    
    
    
    
    
    
    
    
        
        
            
        
        
    
    
    function asignarPorcentaje($db_pdo, $kcvesol, $porcentaje, $kcct, $turno){
    	
        //--realiza update en oporcentajeasignado, isurmodprc='PROCESO' y  ifecmodprc.
        //--encaso de que el porcentaje a asignar sobrepase a los puntos restantes el 
        //  porcentaje se queda con los puntos restantes que tenemos.
        //--actualizamos r3capacidadbeca opuntosasignados y puntosrestantes.
	
        //r3solicitantetutor23082012
        //r3capacidadbecas14082012            
        
            
        $Fec = date('Y-m-d H:i:s');
            
            
        if($kcct=='9013'){
                            
            //hay 2 en capacidadbecas
            $qryPuntos = "SELECT opuntosrestantes_b, opuntosasignados_b as pa
            FROM r3capacidadbecas
            WHERE rcvecct=".$kcct."
            AND rcveturno='".$turno."'
            AND oejercicio=2012";     
                
        }else{
            
            $qryPuntos = "SELECT opuntosrestantes_b, opuntosasignados_b as pa
            FROM r3capacidadbecas
            WHERE rcvecct=".$kcct."
            AND oejercicio=2012";
                
        }
        
            
        
        $rPtos = $db_pdo->query($qryPuntos) or die("Query Puntos capacidad becas Error!! " . $qryPuntos);
        $row = $rPtos->fetch(PDO::FETCH_ASSOC);
                                
                    
        $ptos = $row["OPUNTOSRESTANTES_B"];
        $ptosAsignados = $row["PA"];
                
        if($ptos <= $porcentaje){
            $porcentaje = $ptos;
        } 
                    
        //osinmetadatos = 'S'
                        
        $sql = "UPDATE r3solicitantetutor SET
        oporcentajeasignado_s = ".$porcentaje.",
        iusrmodprc_s = 'PROCESO X',
        ifecmodprc_s = '".$Fec."'
                
        WHERE kcvesolicitante =".$kcvesol."
        AND oejercicio='2012'";
        $db_pdo->query($sql) or die("Upadte Solicitante porcentaje Error!! ... " . $sql);

        $restante = $ptos - $porcentaje;
        $asignado = $ptosAsignados + $porcentaje;

            
            
                
        
        
        if($kcct=='9013'){
                            
            //hay 2 en capacidadbecas  
            $qryup = "UPDATE r3capacidadbecas SET
            opuntosrestantes_b = ".$restante.",
            opuntosasignados_b = ".$asignado."
            WHERE rcvecct = ".$kcct."   
            AND rcveturno='".$turno."'
            AND oejercicio='2012'";
                
                
        }else{
            
            $qryup = "UPDATE r3capacidadbecas SET
            opuntosrestantes_b = ".$restante.",
            opuntosasignados_b = ".$asignado."
            WHERE rcvecct = ".$kcct."
            AND oejercicio='2012'";
       
        }
            
        
            
        
            
        
        
        
        
        $db_pdo->query($qryup) or die("UPDATE CapacidadBecas puntos restantes Error!! ".$qryup);
        
    }
    
    
    
    
        
            
    
        
        
        
        
        
    
    
    
    
    
    
    function printA($g){
        
        //muestra los valores que se tienen de gradocantidad [grado][cantidadbecas][candidatosxgrado][dbpdo]
        foreach ($g as $value) {
            echo $value["grado"] . " : ";
            echo $value["cantidad"] . " : ";
            echo $value["conteo"] . " : ";
            echo $value["status"] . "<br/>";
        }
        
    }
    
    
    
    
    
    
    
    
            
    
    
    
    
    
    
    
            
       
    
    
        
        
        
    
    
        
    
    function asignarPuntosRestantes1($db_pdo){
		 
        
        
        $PORCENTAJE_MIN = 5;
        $Fec = date('Y-m-d H:i:s');
        $cad = 1;
        $ret = "";
            
                
                        
        
        //todos los cct que tengan mas de 0 en puntos restantes
       	$sqlct = "SELECT cap.rcvecct as rcvecct, cap.rcveturno as rcveturno, cap.ototalbecascompletas, cap.ototalpuntos as tot, 
        cap.opuntosrestantes as rest, cap.opuntosasignados as asig, x.oclave, x.ofolioincorporacion
        FROM r3capacidadbecass cap, x1centrotrabajo x
        WHERE (cap.rcvecct = x.kcvect)
        AND cap.oejercicio='2012'
        AND cap.ototalalumnos>=5
        AND cap.opuntosrestantes > 0
                
            
        AND (cap.kcvecapacidad>='14807')
            

        ORDER BY cap.rcvecct";
        
            
            
        $rsltct = $db_pdo->query($sqlct) or die("error Capacidad CAPBEC...");
        while($rowct = $rsltct->fetch(PDO::FETCH_ASSOC)){

                    
		
            $tot = $rowct["TOT"];
            $asig = $rowct["ASIG"];
            $rest = $rowct["REST"];
            $cct = $rowct["RCVECCT"];
			

                    
                    
                
            
            if($rowct["RCVECCT"]=='9013'){
                            
                //traemos todos los solicitantes beneficiados que tiene menos de 100%
                //nota no tomamos en cuenta los casos especiales oce
                $sqlbeneficiados = "SELECT kcvesolicitante, oporcentajeasignado_s as pa,
                oponderaciontotalinicial_s as ponderacion
                FROM r3solicitantetutors
                WHERE obeneficiado_s = 'S'
                AND rcvecctactual_s = ".$rowct["RCVECCT"]."
                AND rcveturno_s='".$rowct["RCVETURNO"]."'
                AND opromedioverificado_s<10
                AND oporcentajeasignado_s > 0
                AND oporcentajeasignado_s < 100
                AND oce is null
                AND oejercicio='2012'
                ORDER BY ponderacion DESC";
                

            }else{

                 //traemos todos los solicitantes beneficiados que tiene menos de 100%
                //nota no tomamos en cuenta los casos especiales oce
                $sqlbeneficiados = "SELECT kcvesolicitante, oporcentajeasignado_s as pa,
                oponderaciontotalinicial_s as ponderacion
                FROM r3solicitantetutors
                WHERE obeneficiado_s = 'S'
                AND rcvecctactual_s = ".$rowct["RCVECCT"]."
                AND opromedioverificado_s<10
                AND oporcentajeasignado_s > 0
                AND oporcentajeasignado_s < 100
                AND oce is null
                AND oejercicio='2012'
                ORDER BY ponderacion DESC";
                
            }
            
                
                    
           

            $i=0;
            $cc=0;
            $rsltbeneficiados = $db_pdo->query($sqlbeneficiados) or die("Query beneficiados error...");
            while ($rowbeneficiados = $rsltbeneficiados->fetch(PDO::FETCH_ASSOC)){
                
                //beneficiados dentro de vector empieza en 1 (termina 6)
                $alumnosbeneficiados[$i]["kcvesol"] = $rowbeneficiados["KCVESOLICITANTE"];
                $alumnosbeneficiados[$i]["porcentaje"] = $rowbeneficiados["PA"];
                $alumnosbeneficiados[$i]["estado"] = '0';
                $i++;
                $cc++;	
                
            }

                
                    
			
            //echo "----------------------------------------------------"."<br>";
            //echo "|| 1A ASIGNACION || "."<br>";
            //echo "CT: ".$cct." || TOT: ".$tot." || ASIG: ".$asig." || REST: ".$rest."<br/>";
            //for($j=0; $j<count($alumnosbeneficiados); $j++){
                //echo $j." KCVE: ".$alumnosbeneficiados[$j]["kcvesol"]." || PTOS: ".$alumnosbeneficiados[$j]["porcentaje"]. " || ESTADO: ".$alumnosbeneficiados[$j]["estado"]."<br>";
            //}
			
                
  
                
                
            
            
            while ($rest>=$PORCENTAJE_MIN && $cc>0){
    			
                for($k=0; $k<count($alumnosbeneficiados); $k++){
				
                    if($rest>=$PORCENTAJE_MIN){
						
                        if($alumnosbeneficiados[$k]["porcentaje"] < 100){
						
                            $alumnosbeneficiados[$k]["porcentaje"] = $alumnosbeneficiados[$k]["porcentaje"] + $PORCENTAJE_MIN;
                            $alumnosbeneficiados[$k]["estado"] = 1;
                            $rest = $rest - $PORCENTAJE_MIN;
                            $asig = $asig + $PORCENTAJE_MIN;
                            
                            if($alumnosbeneficiados[$k]["porcentaje"] == 100){
                                $cc = $cc-1;
                            }   
                            
                        }
                        
                    }else{
                        
                        break;
                        
                    }
                    
                }
                
            }	
			
			
                    
			
            //echo "----------------------------------------------------"."<br>";
            //echo "|| 2A ASIGNACION || "."<br>";
            //echo "CT: ".$cct." || TOT: ".$tot." || ASIG: ".$asig." || REST: ".$rest."<br/>";
            //for($m=0; $m<count($alumnosbeneficiados); $m++){
                //echo $m." KCVE: ".$alumnosbeneficiados[$m]["kcvesol"]." || PTOS: ".$alumnosbeneficiados[$m]["porcentaje"]. " || ESTADO: ".$alumnosbeneficiados[$m]["estado"]."<br>";
            //}
			
			
			
			
            

            
                
                    
                
			
			
            //ACTUALIZACIONES
            //actualizamos r3capacidadbecas
            $sqlcap = "UPDATE r3capacidadbecass SET
            opuntosrestantes=".$rest.", 
            opuntosasignados=".$asig."
            WHERE oejercicio='2012'
            AND rcvecct='".$rowct["RCVECCT"]."' 
            AND rcveturno = '".$rowct["RCVETURNO"]."'";

            $rsltcap = $db_pdo->query($sqlcap) or die ("query error cap".$sqlcap);
            $rowcap = $rsltcap->fetch(PDO::FETCH_ASSOC);
				
                        
                    
                    
                
                    
            
            //actualizamos beneficiados
            for($n=0; $n<count($alumnosbeneficiados); $n++){
                
                    
                            
                $sqlbenf = "UPDATE r3solicitantetutors SET 
                oporcentajeasignado_s =".$alumnosbeneficiados[$n]["porcentaje"].",
                iusrmodprc_s ='PROCESO REST',
                ifecmodprc_s ='".$Fec."'
                WHERE kcvesolicitante='".$alumnosbeneficiados[$n]["kcvesol"]."'
                AND oejercicio='2012' "; 
                    
                
                $rsltbenf = $db_pdo->query($sqlbenf) or die ("query error benf".$sqlbenf);
                $rowbenf = $rsltbenf->fetch(PDO::FETCH_ASSOC);

            }
			
                
            
                
                
			
            //vaciamos el vector para la siguiente escuela
            unset($alumnosbeneficiados); 
            
            
            $ret = $ret." ".$cad." CCT:".$rowct["RCVECCT"]." AFECTADOS: ".$i."<br>";
            $cad++;
                
				
        }       
		
        return $ret;
            
	
    }
    
    
    
    
            
        
    
    
        
    
    
    
    
}
?>
