// JavaScript Document
//*********************************

//*********************************

//*********************************
//Cambia el color del borde de los elementos de un formulario
//recibe el elemento y el evento (focus o blur)
//*********************************
function validarletras22(e) {
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==8 || tecla==0) return true;
    patron =/[A-Za-z\s\áéíóúÁÉÍÓÚñÑüÜ]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}  



function cambiarBorde(elemento,opcion){

	if(opcion==1){
	
		elemento.style.borderColor="#69BE30";
		elemento.style.borderTopColor="#5aa92a";
		elemento.style.borderWidth="3px";
	}
	if(opcion==0){

		elemento.style.borderColor="#CCCCCC";
		elemento.style.borderTopColor="#787878";
		elemento.style.borderWidth="1px";
	}

}

//*********************************
//Deshabilita todos los elementos de un formulario
//@param formulario el formulario a deshabilitar
//*********************************
function deshabilitarFormulario(formulario){
	for(i=0; i<formulario.elements.length; i++){
		formulario.elements[i].disabled = true;
	}
}


//*********************************
//Obtiene todos los elementos por el nombre de la clase
//*********************************
document.getElementsByClassName = function(cl) {
    var retnode = [];
    var myclass = new RegExp('\\b'+cl+'\\b');
    var elem = this.getElementsByTagName('*');
    for (var i = 0; i < elem.length; i++) {
        var classes = elem[i].className;
        if (myclass.test(classes)) retnode.push(elem[i]);
    }
    return retnode;
};


//*********************************
//Función ajax
//*********************************
function nuevoAjax(){
      /* Crea el objeto AJAX. Esta funcion es generica para cualquier utilidad de este tipo, por
      lo que se puede copiar tal como esta aqui */
      var xmlhttp=false;
      try
      {
         // Creacion del objeto AJAX para navegadores no IE
         xmlhttp=new ActiveXObject("Msxml2.XMLHTTP");
      }
      catch(e)
      {
         try
         {
            // Creacion del objet AJAX para IE
            xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
         }
         catch(E) { xmlhttp=false; }
      }
      if (!xmlhttp && typeof XMLHttpRequest!="undefined") { xmlhttp=new XMLHttpRequest(); }

      return xmlhttp;
}

//*********************************
//Función que pasa a mayusculas todo el texto
//*********************************

function pasarMayusculas(textfield){
	textfield.value = textfield.value.toUpperCase();	
}


//*******************************************
//Función que Muestra los TR de una tabla
//*******************************************
function Muestra(FilaMuestra) {
	var ElementosMuestra = document.getElementsByName(FilaMuestra);

		for (i = 0; i< ElementosMuestra.length; i++) {
			if(navigator.appName.indexOf("Microsoft") > -1){
			   var visible = 'block'
			} else {
			   var visible = 'table-row';
			}
			ElementosMuestra[i].style.display = visible;
		}
}

//*******************************************
//Función que Oculta los TR de una tabla
//*******************************************
function Oculta(FilaOculta) {
	var ElementosOculta = document.getElementsByName(FilaOculta);
	
		for (j = 0; j< ElementosOculta.length; j++) {
			ElementosOculta[j].style.display = "none";
		}	
}

//**************************************************
//Función que Muestra y oculta los TR de una tabla
//**************************************************
function MuestrayOculta(FilaMuestra, FilaOculta) {
	var ElementosMuestra = document.getElementsByName(FilaMuestra);
	var ElementosOculta = document.getElementsByName(FilaOculta);

		for (i = 0; i< ElementosMuestra.length; i++) {
			if(navigator.appName.indexOf("Microsoft") > -1){
			   var visible = 'block'
			} else {
			   var visible = 'table-row';
			}
			ElementosMuestra[i].style.display = visible;
		}

		for (j = 0; j< ElementosOculta.length; j++) {
			ElementosOculta[j].style.display = "none";
		}	
}

//**************************************************
//Función que Muestra1 y oculta4  TR de una tabla
//**************************************************
function MuestrayOculta4(FilaMuestra1, FilaOculta2, FilaOculta3, FilaOculta4) {
	var ElementosMuestra1 = document.getElementsByName(FilaMuestra1);
	var ElementosOculta2 = document.getElementsByName(FilaOculta2);
	var ElementosOculta3 = document.getElementsByName(FilaOculta3);
	var ElementosOculta4 = document.getElementsByName(FilaOculta4);
	

		for (i = 0; i< ElementosMuestra1.length; i++) {
			if(navigator.appName.indexOf("Microsoft") > -1){
			   var visible = 'block'
			} else {
			   var visible = 'table-row';
			}
			ElementosMuestra1[i].style.display = visible;
		}

		for (j = 0; j< ElementosOculta2.length; j++) {
			ElementosOculta2[j].style.display = "none";
		}
		for (j = 0; j< ElementosOculta3.length; j++) {
			ElementosOculta3[j].style.display = "none";
		}
		for (j = 0; j< ElementosOculta4.length; j++) {
			ElementosOculta4[j].style.display = "none";
		}	
		
}


//*******************************************************************//
//Función que Muestra1 y oculta  TR de de cedula
//*******************************************************************//
	function muestra_cedula(elemento) {
		if(elemento.value==2) {  //si selecciona escritura		

				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("filacedula").style.display = "block";
				}else{
					document.getElementById("filacedula").style.display = "table-row";	
				}			
			
		}else{
			document.getElementById("filacedula").style.display = "none";	
		}
	} 
//*******************************************************************//
//Función que Muestra1 y oculta  TR de una tabla					 //
//*******************************************************************//
	function muestra_oculta(elemento) {
		if(elemento.value=='') {  //si no selecciona nada
			for (i = 1; i<= 8; i++) {
				document.getElementById("InmuebleEscritura"+i).style.display = "none";	
			}				
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}			
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}		
			document.getElementById("VigenciaArrendamiento").style.display = "none";	
			document.getElementById("VigenciaComodato").style.display = "none";	

			
		}
		if(elemento.value=='Escritura Publica de Propiedad') {  //si selecciona escritura		
			for (i = 1; i<= 8; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleEscritura"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleEscritura"+i).style.display = "table-row";	
				}			
			}				
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}
			document.getElementById("VigenciaArrendamiento").style.display = "none";	
			document.getElementById("VigenciaComodato").style.display = "none";	
		}
		//Contrato de Arrendamiento Contrato Comodato Otro
		if(elemento.value=='Contrato de Arrendamiento') {  //si selecciona arrendamiento		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleArrendamiento"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleArrendamiento"+i).style.display = "table-row";	
				}			
			}				
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleComodato"+k).style.display = "none";	
			}		
			for (l = 1; l<= 2; l++) {
				document.getElementById("InmuebleOtros"+l).style.display = "none";	
			}
			document.getElementById("VigenciaComodato").style.display = "none";	
		}
		
		if(elemento.value=='Contrato Comodato') {  //si selecciona comodato		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleComodato"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleComodato"+i).style.display = "table-row";	
				}			
			}				
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleArrendamiento"+k).style.display = "none";	
			}		
			for (l = 1; l<= 2; l++) {
				document.getElementById("InmuebleOtros"+l).style.display = "none";	
			}
			document.getElementById("VigenciaArrendamiento").style.display = "none";	
		}
		
		if(elemento.value=='Otro') {  //si selecciona otros 
			for (i = 1; i<= 2; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleOtros"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleOtros"+i).style.display = "table-row";	
				}			
			}				
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleArrendamiento"+k).style.display = "none";	
				document.getElementById("InmuebleComodato"+k).style.display = "none";	
			}
			document.getElementById("VigenciaArrendamiento").style.display = "none";	
			document.getElementById("VigenciaComodato").style.display = "none";			
		}
		
		 
	}
	
	
	
//**************************************************
//Función que Muestra1  al iniciar una pagina los  TR de una tabla
//**************************************************
function muestra_oculta2(elemento) {
		
		
		if(document.getElementById(elemento).value=='') {  //si no selecciona nada
			for (i = 1; i<= 8; i++) {
				document.getElementById("InmuebleEscritura"+i).style.display = "none";	
			}				
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}			
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}		
			document.getElementById("VigenciaArrendamiento").style.display = "none";	
			document.getElementById("VigenciaComodato").style.display = "none";	
			
		}
		
		if(document.getElementById(elemento).value=='Escritura Publica de Propiedad') {  //si selecciona escritura		
			for (i = 1; i<= 8; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleEscritura"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleEscritura"+i).style.display = "table-row";	
				}			
			}				
	
		}
		//Contrato de Arrendamiento Contrato Comodato Otro
		if(document.getElementById(elemento).value=='Contrato de Arrendamiento') {  //si selecciona arrendamiento		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleArrendamiento"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleArrendamiento"+i).style.display = "table-row";	
				}			
			}				
			
		}
		
		
		if(document.getElementById(elemento).value=='Contrato Comodato') {  //si selecciona comodato		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleComodato"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleComodato"+i).style.display = "table-row";	
				}			
			}				
		}
		
		if(document.getElementById(elemento).value=='Otro') {  //si selecciona otros 
			for (i = 1; i<= 2; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleOtros"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleOtros"+i).style.display = "table-row";	
				}			
			}				
		}
		
		
	}	
	
	
//**************************************************
//Función que valida tabla de otros niveles incorporados
//**************************************************	
	function validarformulario10(FrmDat){
		
		if(FrmDat.RadioPreg12[0].checked==false && FrmDat.RadioPreg12[1].checked==false){
			alert('Debe seleccionar si cuenta con otro nivel incorporado a SEIEM');
			return false;	
		}


		if(FrmDat.RadioPreg12[0].checked==true){
		//SI TIENE OTROS NIVELES INCORPORADOS
			if(FrmDat.selectnivel1.value != '' || FrmDat.Txtcct1.value != '' || FrmDat.TextAreanombrecct1.value != '' || FrmDat.TextAreaubicacion1.value != '' || FrmDat.Radiotiponivel1[0].checked!=false || FrmDat.Radiotiponivel1[1].checked!=false || FrmDat.Radiotiponivel1[2].checked!=false){
			
				if(FrmDat.selectnivel1.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel1.focus(); 
				   FrmDat.selectnivel1.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel1.style.backgroundColor = "#FFFFFF";
				
				
				if(FrmDat.selectnivel1.value == 'EDI'){
				  if(FrmDat.TextAreaubicacion1.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion1.focus(); 
					   FrmDat.TextAreaubicacion1.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion1.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel1[0].checked==false && FrmDat.Radiotiponivel1[1].checked==false && FrmDat.Radiotiponivel1[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
				
				if(FrmDat.selectnivel1.value != 'EDI' && FrmDat.selectnivel1.value != ''){
				
					if(FrmDat.Txtcct1.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct1.focus(); 
					   FrmDat.Txtcct1.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct1.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct1.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct1.focus(); 
					   FrmDat.TextAreanombrecct1.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct1.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion1.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion1.focus(); 
					   FrmDat.TextAreaubicacion1.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion1.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel1[0].checked==false && FrmDat.Radiotiponivel1[1].checked==false && FrmDat.Radiotiponivel1[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				
				}
				
			}
			
			/**2**/	
			if(FrmDat.selectnivel2.value != '' || FrmDat.Txtcct2.value != '' || FrmDat.TextAreanombrecct2.value != '' || FrmDat.TextAreaubicacion2.value != '' || FrmDat.Radiotiponivel2[0].checked!=false || FrmDat.Radiotiponivel2[1].checked!=false || FrmDat.Radiotiponivel2[2].checked!=false){
			
				if(FrmDat.selectnivel2.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel2.focus(); 
				   FrmDat.selectnivel2.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel2.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel2.value == 'EDI'){
					if(FrmDat.TextAreaubicacion2.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion2.focus(); 
					   FrmDat.TextAreaubicacion2.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion2.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel2[0].checked==false && FrmDat.Radiotiponivel2[1].checked==false && FrmDat.Radiotiponivel2[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}	
				}
				
				if(FrmDat.selectnivel2.value != 'EDI' && FrmDat.selectnivel2.value != ''){
					if(FrmDat.Txtcct2.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct2.focus(); 
					   FrmDat.Txtcct2.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct2.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct2.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct2.focus(); 
					   FrmDat.TextAreanombrecct2.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct2.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion2.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion2.focus(); 
					   FrmDat.TextAreaubicacion2.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion2.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel2[0].checked==false && FrmDat.Radiotiponivel2[1].checked==false && FrmDat.Radiotiponivel2[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
	
			}
			/**/
			
			/**3**/	
			if(FrmDat.selectnivel3.value != '' || FrmDat.Txtcct3.value != '' || FrmDat.TextAreanombrecct3.value != '' || FrmDat.TextAreaubicacion3.value != '' || FrmDat.Radiotiponivel3[0].checked!=false || FrmDat.Radiotiponivel3[1].checked!=false || FrmDat.Radiotiponivel3[2].checked!=false){
			
				if(FrmDat.selectnivel3.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel3.focus(); 
				   FrmDat.selectnivel3.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel3.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel3.value == 'EDI'){
					if(FrmDat.TextAreaubicacion3.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion3.focus(); 
					   FrmDat.TextAreaubicacion3.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion3.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel3[0].checked==false && FrmDat.Radiotiponivel3[1].checked==false && FrmDat.Radiotiponivel3[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}	
				}
				
				
				if(FrmDat.selectnivel3.value != 'EDI' && FrmDat.selectnivel3.value != ''){
					if(FrmDat.Txtcct3.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct3.focus(); 
					   FrmDat.Txtcct3.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct3.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct3.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct3.focus(); 
					   FrmDat.TextAreanombrecct3.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct3.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion3.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion3.focus(); 
					   FrmDat.TextAreaubicacion3.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion3.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel3[0].checked==false && FrmDat.Radiotiponivel3[1].checked==false && FrmDat.Radiotiponivel3[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			
			/**4**/	
			if(FrmDat.selectnivel4.value != '' || FrmDat.Txtcct4.value != '' || FrmDat.TextAreanombrecct4.value != '' || FrmDat.TextAreaubicacion4.value != '' || FrmDat.Radiotiponivel4[0].checked!=false || FrmDat.Radiotiponivel4[1].checked!=false || FrmDat.Radiotiponivel4[2].checked!=false){
			
				if(FrmDat.selectnivel4.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel4.focus(); 
				   FrmDat.selectnivel4.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel4.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel4.value == 'EDI'){
					if(FrmDat.TextAreaubicacion4.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion4.focus(); 
					   FrmDat.TextAreaubicacion4.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion4.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel4[0].checked==false && FrmDat.Radiotiponivel4[1].checked==false && FrmDat.Radiotiponivel4[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}	
				}
				
				
				if(FrmDat.selectnivel4.value != 'EDI' && FrmDat.selectnivel4.value != ''){
					if(FrmDat.Txtcct4.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct4.focus(); 
					   FrmDat.Txtcct4.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct4.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct4.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct4.focus(); 
					   FrmDat.TextAreanombrecct4.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct4.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion4.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion4.focus(); 
					   FrmDat.TextAreaubicacion4.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion4.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel4[0].checked==false && FrmDat.Radiotiponivel4[1].checked==false && FrmDat.Radiotiponivel4[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			/**5**/	
			if(FrmDat.selectnivel5.value != '' || FrmDat.Txtcct5.value != '' || FrmDat.TextAreanombrecct5.value != '' || FrmDat.TextAreaubicacion5.value != '' || FrmDat.Radiotiponivel5[0].checked!=false || FrmDat.Radiotiponivel5[1].checked!=false || FrmDat.Radiotiponivel5[2].checked!=false){
			
				if(FrmDat.selectnivel5.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel5.focus(); 
				   FrmDat.selectnivel5.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel5.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel5.value == 'EDI'){
					if(FrmDat.TextAreaubicacion5.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion5.focus(); 
					   FrmDat.TextAreaubicacion5.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion5.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel5[0].checked==false && FrmDat.Radiotiponivel5[1].checked==false && FrmDat.Radiotiponivel5[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
				   
				if(FrmDat.selectnivel5.value != 'EDI' && FrmDat.selectnivel5.value != ''){
				
					if(FrmDat.Txtcct5.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct5.focus(); 
					   FrmDat.Txtcct5.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct5.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct5.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct5.focus(); 
					   FrmDat.TextAreanombrecct5.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct5.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion5.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion5.focus(); 
					   FrmDat.TextAreaubicacion5.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion5.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel5[0].checked==false && FrmDat.Radiotiponivel5[1].checked==false && FrmDat.Radiotiponivel5[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			/**6**/	
			if(FrmDat.selectnivel6.value != '' || FrmDat.Txtcct6.value != '' || FrmDat.TextAreanombrecct6.value != '' || FrmDat.TextAreaubicacion6.value != '' || FrmDat.Radiotiponivel6[0].checked!=false || FrmDat.Radiotiponivel6[1].checked!=false || FrmDat.Radiotiponivel6[2].checked!=false){
			
				if(FrmDat.selectnivel6.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel6.focus(); 
				   FrmDat.selectnivel6.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel6.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel6.value == 'EDI'){
					if(FrmDat.TextAreaubicacion6.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion6.focus(); 
					   FrmDat.TextAreaubicacion6.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion6.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel6[0].checked==false && FrmDat.Radiotiponivel6[1].checked==false && FrmDat.Radiotiponivel6[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
				
				if(FrmDat.selectnivel6.value != 'EDI' && FrmDat.selectnivel6.value != ''){
					if(FrmDat.Txtcct6.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct6.focus(); 
					   FrmDat.Txtcct6.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct6.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct6.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct6.focus(); 
					   FrmDat.TextAreanombrecct6.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct6.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion6.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion6.focus(); 
					   FrmDat.TextAreaubicacion6.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion6.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel6[0].checked==false && FrmDat.Radiotiponivel6[1].checked==false && FrmDat.Radiotiponivel6[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			
			/**7**/	
			if(FrmDat.selectnivel7.value != '' || FrmDat.Txtcct7.value != '' || FrmDat.TextAreanombrecct7.value != '' || FrmDat.TextAreaubicacion7.value != '' || FrmDat.Radiotiponivel7[0].checked!=false || FrmDat.Radiotiponivel7[1].checked!=false || FrmDat.Radiotiponivel7[2].checked!=false){
			
				if(FrmDat.selectnivel7.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel7.focus(); 
				   FrmDat.selectnivel7.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel7.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel7.value == 'EDI'){
					if(FrmDat.TextAreaubicacion7.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion7.focus(); 
					   FrmDat.TextAreaubicacion7.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion7.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel7[0].checked==false && FrmDat.Radiotiponivel7[1].checked==false && FrmDat.Radiotiponivel7[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}	
				}
				
				
				if(FrmDat.selectnivel7.value != 'EDI' && FrmDat.selectnivel7.value != ''){
					if(FrmDat.Txtcct7.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct7.focus(); 
					   FrmDat.Txtcct7.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct7.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct7.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct7.focus(); 
					   FrmDat.TextAreanombrecct7.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct7.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion7.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion7.focus(); 
					   FrmDat.TextAreaubicacion7.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion7.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel7[0].checked==false && FrmDat.Radiotiponivel7[1].checked==false && FrmDat.Radiotiponivel7[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			
			/**8**/	
			if(FrmDat.selectnivel8.value != '' || FrmDat.Txtcct8.value != '' || FrmDat.TextAreanombrecct8.value != '' || FrmDat.TextAreaubicacion8.value != '' || FrmDat.Radiotiponivel8[0].checked!=false || FrmDat.Radiotiponivel8[1].checked!=false || FrmDat.Radiotiponivel8[2].checked!=false){
			
				if(FrmDat.selectnivel8.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel8.focus(); 
				   FrmDat.selectnivel8.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel8.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel8.value == 'EDI'){
					if(FrmDat.TextAreaubicacion8.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion8.focus(); 
					   FrmDat.TextAreaubicacion8.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion8.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel8[0].checked==false && FrmDat.Radiotiponivel8[1].checked==false && FrmDat.Radiotiponivel8[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
					
				}
				
				if(FrmDat.selectnivel8.value != 'EDI' && FrmDat.selectnivel8.value != ''){
					if(FrmDat.Txtcct8.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct8.focus(); 
					   FrmDat.Txtcct8.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct8.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct8.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct8.focus(); 
					   FrmDat.TextAreanombrecct8.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct8.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion8.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion8.focus(); 
					   FrmDat.TextAreaubicacion8.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion8.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel8[0].checked==false && FrmDat.Radiotiponivel8[1].checked==false && FrmDat.Radiotiponivel8[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			
			/**9**/	
			if(FrmDat.selectnivel9.value != '' || FrmDat.Txtcct9.value != '' || FrmDat.TextAreanombrecct9.value != '' || FrmDat.TextAreaubicacion9.value != '' || FrmDat.Radiotiponivel9[0].checked!=false || FrmDat.Radiotiponivel9[1].checked!=false || FrmDat.Radiotiponivel9[2].checked!=false){
			
				if(FrmDat.selectnivel9.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel9.focus(); 
				   FrmDat.selectnivel9.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel9.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel9.value == 'EDI' ){
					if(FrmDat.TextAreaubicacion9.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion9.focus(); 
					   FrmDat.TextAreaubicacion9.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion9.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel9[0].checked==false && FrmDat.Radiotiponivel9[1].checked==false && FrmDat.Radiotiponivel9[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
				
				if(FrmDat.selectnivel9.value != 'EDI' && FrmDat.selectnivel9.value != ''){
					if(FrmDat.Txtcct9.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct9.focus(); 
					   FrmDat.Txtcct9.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct9.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct9.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct9.focus(); 
					   FrmDat.TextAreanombrecct9.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct9.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion9.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion9.focus(); 
					   FrmDat.TextAreaubicacion9.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion9.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel9[0].checked==false && FrmDat.Radiotiponivel9[1].checked==false && FrmDat.Radiotiponivel9[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
	
			}
			/**/
			
			
			/**10**/	
			if(FrmDat.selectnivel10.value != '' || FrmDat.Txtcct10.value != '' || FrmDat.TextAreanombrecct10.value != '' || FrmDat.TextAreaubicacion10.value != '' || FrmDat.Radiotiponivel10[0].checked!=false || FrmDat.Radiotiponivel10[1].checked!=false || FrmDat.Radiotiponivel10[2].checked!=false){
			
				if(FrmDat.selectnivel10.value == ''){
				   alert('Debe seleccionar el nivel del centro de trabajo');
				   FrmDat.selectnivel10.focus(); 
				   FrmDat.selectnivel10.style.backgroundColor = "#CBECB5";
				   return false;
				}
				FrmDat.selectnivel10.style.backgroundColor = "#FFFFFF";
				
				if(FrmDat.selectnivel10.value == 'EDI'){
					if(FrmDat.TextAreaubicacion10.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion10.focus(); 
					   FrmDat.TextAreaubicacion10.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion10.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel10[0].checked==false && FrmDat.Radiotiponivel10[1].checked==false && FrmDat.Radiotiponivel10[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}	
				}
				
				if(FrmDat.selectnivel10.value != 'EDI' && FrmDat.selectnivel10.value != ''){
					if(FrmDat.Txtcct10.value == ''){
					   alert('Debe registrar la clave del centro de trabajo');
					   FrmDat.Txtcct10.focus(); 
					   FrmDat.Txtcct10.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.Txtcct10.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreanombrecct10.value == ''){
					   alert('Debe registrar el nombre del centro de trabajo');
					   FrmDat.TextAreanombrecct10.focus(); 
					   FrmDat.TextAreanombrecct10.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreanombrecct10.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.TextAreaubicacion10.value == ''){
					   alert('Debe registrar la ubicacion y telefono del centro de trabajo');
					   FrmDat.TextAreaubicacion10.focus(); 
					   FrmDat.TextAreaubicacion10.style.backgroundColor = "#CBECB5";
					   return false;
					}
					FrmDat.TextAreaubicacion10.style.backgroundColor = "#FFFFFF";
					
					if(FrmDat.Radiotiponivel10[0].checked==false && FrmDat.Radiotiponivel10[1].checked==false && FrmDat.Radiotiponivel10[2].checked==false){
						alert('Debe seleccionar a que subsistema pertenece el centro de trabajo');
						return false;	
					}
				}
			}
			/**/
			
			
		}
		
		
		return true;
	}
	





//*******************************************************************//
//Función que Muestra1 y oculta  TR de una tabla	 5				 //
//*******************************************************************//

	function muestra_oculta5(elemento, val_arr, val_com) {
		
		if(elemento.value=='') {  //si no selecciona nada
			for (i = 1; i<= 8; i++) {
				document.getElementById("InmuebleEscritura"+i).style.display = "none";	
			}				
			
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}		
			
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}
			
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";	
		}
		
		
		
		if(elemento.value=='ESCRITURA PUBLICA DE PROPIEDAD') {  //si selecciona escritura		
			for (i = 1; i<= 8; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleEscritura"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleEscritura"+i).style.display = "table-row";	
				}			
			}
			
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}
			
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}
			
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";	

		}
		//Contrato de Arrendamiento Contrato Comodato Otro
		if(elemento.value=='CONTRATO ARRENDAMIENTO') {  //si selecciona arrendamiento		
			
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleArrendamiento"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleArrendamiento"+i).style.display = "table-row";	
				}			
			}
				if(val_arr=='S' && val_com=='F'){
			
					if(navigator.appName.indexOf("Microsoft") > -1){
						document.getElementById("Vigencia_arr").style.display = "block";
					}else{
						document.getElementById("Vigencia_arr").style.display = "table-row";	
					}
			
				}
				
				if(val_arr=='N' && val_com=='F'){
					document.getElementById("Vigencia_arr").style.display = "none";		
				}
				
				
			
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleComodato"+k).style.display = "none";	
			}		
			
			for (l = 1; l<= 2; l++) {
				document.getElementById("InmuebleOtros"+l).style.display = "none";	
			}
			
			document.getElementById("Vigencia_com").style.display = "none";	

		}
		
		if(elemento.value=='CONTRATO COMODATO') {  //si selecciona comodato		
			
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleComodato"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleComodato"+i).style.display = "table-row";	
				}			
			}	
			
				if(val_com=='S' && val_arr=='F'){	
					if(navigator.appName.indexOf("Microsoft") > -1){
						document.getElementById("Vigencia_com").style.display = "block";
					}else{
						document.getElementById("Vigencia_com").style.display = "table-row";	
					}
				}
				if(val_com=='N' && val_arr=='F'){
					document.getElementById("Vigencia_com").style.display = "none";			
				}
			
			
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleArrendamiento"+k).style.display = "none";	
			}		
			
			for (l = 1; l<= 2; l++) {
				document.getElementById("InmuebleOtros"+l).style.display = "none";	
			}
			
			
			document.getElementById("Vigencia_arr").style.display = "none";	
			
		}
		
		if(elemento.value=='OTRO INMUEBLE') {  //si selecciona otros 
			
			for (i = 1; i<= 2; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleOtros"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleOtros"+i).style.display = "table-row";	
				}			
			}				
			for (j = 1; j<= 8; j++) {
				document.getElementById("InmuebleEscritura"+j).style.display = "none";	
			}
			
			for (k = 1; k<= 10; k++) {
				document.getElementById("InmuebleArrendamiento"+k).style.display = "none";	
				document.getElementById("InmuebleComodato"+k).style.display = "none";	
			}
			
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";	
			
		}
		
		 
	}
	  

//*******************************************************************//
//Función que Muestra1 y oculta  TR de una tabla	 6				 //
//*******************************************************************//

	function muestra_oculta6(elemento, val_arr, val_com) {
		

		if(document.getElementById(elemento).value=='') {  //si no selecciona nada
			for (i = 1; i<= 8; i++) {
				document.getElementById("InmuebleEscritura"+i).style.display = "none";	
			}				
			for (j = 1; j<= 10; j++) {
				document.getElementById("InmuebleArrendamiento"+j).style.display = "none";	
				document.getElementById("InmuebleComodato"+j).style.display = "none";	
			}			
			for (k = 1; k<= 2; k++) {
				document.getElementById("InmuebleOtros"+k).style.display = "none";	
			}		
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";		
		}
		
		if(document.getElementById(elemento).value=='ESCRITURA PUBLICA DE PROPIEDAD') {  //si selecciona escritura		
			for (i = 1; i<= 8; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleEscritura"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleEscritura"+i).style.display = "table-row";	
				}			
			}
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";	
	
		}
		

		if(document.getElementById(elemento).value=='CONTRATO ARRENDAMIENTO') {  //si selecciona arrendamiento		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleArrendamiento"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleArrendamiento"+i).style.display = "table-row";	
				}			
			}
			
				if(val_arr=='S' && val_com=='F'){
			
					if(navigator.appName.indexOf("Microsoft") > -1){
						document.getElementById("Vigencia_arr").style.display = "block";
					}else{
						document.getElementById("Vigencia_arr").style.display = "table-row";	
					}
			
				}
				
				if(val_arr=='N' && val_com=='F'){
					document.getElementById("Vigencia_arr").style.display = "none";		
				}
			
			
			document.getElementById("Vigencia_com").style.display = "none";	
			
		}
		
		
		if(document.getElementById(elemento).value=='CONTRATO COMODATO') {  //si selecciona comodato		
			for (i = 1; i<= 10; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleComodato"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleComodato"+i).style.display = "table-row";	
				}			
			}
			
			
				if(val_com=='S' && val_arr=='F'){	
					if(navigator.appName.indexOf("Microsoft") > -1){
						document.getElementById("Vigencia_com").style.display = "block";
					}else{
						document.getElementById("Vigencia_com").style.display = "table-row";	
					}
				}
				if(val_com=='N' && val_arr=='F'){
					document.getElementById("Vigencia_com").style.display = "none";			
				}
			
			
			document.getElementById("Vigencia_arr").style.display = "none";	
		}
		
		if(document.getElementById(elemento).value=='OTRO INMUEBLE') {  //si selecciona otros 
			for (i = 1; i<= 2; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("InmuebleOtros"+i).style.display = "block";
				}else{
					document.getElementById("InmuebleOtros"+i).style.display = "table-row";	
				}			
			}		
			document.getElementById("Vigencia_arr").style.display = "none";	
			document.getElementById("Vigencia_com").style.display = "none";	
		}
		
		
	}	

	
	function muestra_mun(elemento) {
		if(elemento.value!='' && elemento.value!='15') {  //si selecciono alguna entidad federativa
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("Mun").style.display = "block";
			}else{
				document.getElementById("Mun").style.display = "table-row";	
			}					
			document.getElementById("Mun2").style.display = "none";	
		}
		
		if(elemento.value=='') {
			document.getElementById("Mun").style.display = "none";	
			document.getElementById("Mun2").style.display = "none";	
		}

		if(elemento.value=='15') {  //si selecciono alguna entidad federativa
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("Mun2").style.display = "block";
			}else{
				document.getElementById("Mun2").style.display = "table-row";	
			}					
			document.getElementById("Mun").style.display = "none";	
		}

	}
	/***************/
	
	function cargarPromedios(selectGrado,nivel){
		var idGrado = selectGrado.value;
		if(idGrado==''){
			document.getElementById("1ro").style.display = "none";	
			document.getElementById("2do").style.display = "none";	
		}
		if(nivel=="PJN"){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("1ro").style.display = "block";
			}else{
				document.getElementById("1ro").style.display = "table-row";	
			}			
			document.getElementById("2do").style.display = "none";	
		}
		
		
		if(idGrado==1 && nivel=="PPR"){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("1ro").style.display = "block";
			}else{
				document.getElementById("1ro").style.display = "table-row";	
			}			
			document.getElementById("2do").style.display = "none";	
		}
		
		
		if(idGrado>=2 && idGrado<=6 && nivel=="PPR"){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("2do").style.display = "block";
			}else{
				document.getElementById("2do").style.display = "table-row";	
			}			
			document.getElementById("1ro").style.display = "none";	
		}
		
		if(nivel=="PES" || nivel=="PST"){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("2do").style.display = "block";
			}else{
				document.getElementById("2do").style.display = "table-row";	
			}			
			document.getElementById("1ro").style.display = "none";	
		}
		
	}
	
	
	function validar(e) {
		tecla = (document.all)?e.keyCode:e.which;
		if (tecla==8) return true;
		if (tecla==0) return true;
		patron = /\d/;
		te = String.fromCharCode(tecla);
		return patron.test(te);
	}
	
	function cargarDatos(selectDato){
		var idDato = selectDato.value;
		if(idDato=='P'){
			for (i = 1; i<= 3; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("padre"+i).style.display = "block";
				}else{
					document.getElementById("padre"+i).style.display = "table-row";	
				}			
			}
			for (i = 1; i<= 3; i++) {
				document.getElementById("madre"+i).style.display = "none";	
				document.getElementById("tutor"+i).style.display = "none";	
			}
                        document.getElementById("sexo1").style.display = "none";
		}
		if(idDato=='M'){
			for (i = 1; i<= 3; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("madre"+i).style.display = "block";
				}else{
					document.getElementById("madre"+i).style.display = "table-row";	
				}			
			}
			for (i = 1; i<= 3; i++) {
				document.getElementById("padre"+i).style.display = "none";	
				document.getElementById("tutor"+i).style.display = "none";	
			}
                        document.getElementById("sexo1").style.display = "none";
		}
		if(idDato=='A'){
			for (i = 1; i<= 3; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("padre"+i).style.display = "block";
				}else{
					document.getElementById("padre"+i).style.display = "table-row";	
				}			
			}
			for (i = 1; i<= 3; i++) {
				document.getElementById("madre"+i).style.display = "none";	
				document.getElementById("tutor"+i).style.display = "none";	
			}
                        document.getElementById("sexo1").style.display = "none";
		}
		if(idDato=='T'){
			for (i = 1; i<= 3; i++) {
				if(navigator.appName.indexOf("Microsoft") > -1){
					document.getElementById("tutor"+i).style.display = "block";
                                        document.getElementById("sexo1").style.display = "block";
				}else{
					document.getElementById("tutor"+i).style.display = "table-row";	
                                        document.getElementById("sexo1").style.display = "table-row";
				}			
			}
			for (i = 1; i<= 3; i++) {
				document.getElementById("padre"+i).style.display = "none";	
				document.getElementById("madre"+i).style.display = "none";	
			}
                        
                            
                        
                        
		}
	}
	
	function MuestraOcultadat(radio, muestra){
		var idradio = radio.value;
		if(idradio=='S'){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById(muestra).style.display = "block";
			}else{
				document.getElementById(muestra).style.display = "table-row";	
			}			
		}
		if(idradio=='N'){
			document.getElementById(muestra).style.display = "none";	
		}
	
	}
	
	
	function MuestraO2(radio2){
		var idradio = radio2.value;
		if(idradio=='S'){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("ingresos1").style.display = "block";
			}else{
				document.getElementById("ingresos1").style.display = "table-row";	
			}			
			document.getElementById("ingresos2").style.display = "none";		
		}
		if(idradio=='N'){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("ingresos2").style.display = "block";
			}else{
				document.getElementById("ingresos2").style.display = "table-row";	
			}			
			document.getElementById("ingresos1").style.display = "none";		
		}
	}
	
	
	
	function MuestraO22(radio2){
		var idradio = radio2.value;
		if(idradio=='S'){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("ingresos1").style.display = "block";
			}else{
				document.getElementById("ingresos1").style.display = "table-row";	
			}			
			document.getElementById("ingresos22").style.display = "none";		
		}
		if(idradio=='N'){
			if(navigator.appName.indexOf("Microsoft") > -1){
				document.getElementById("ingresos22").style.display = "block";
			}else{
				document.getElementById("ingresos22").style.display = "table-row";	
			}			
			document.getElementById("ingresos1").style.display = "none";		
		}
	}