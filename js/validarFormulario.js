function validarFrmLog(FrmLog){
	if(FrmLog.TxtCct.value == ''){
            alert('Por favor capture su CLAVE DE CENTRO DE TRABAJO');
            FrmLog.TxtCct.focus();
            return false;
	}
	if(FrmLog.TxtPsw.value == ''){
            alert('Por Favor capture su CONTRASEÑA');
            FrmLog.TxtPsw.focus();
            return false;
	}
	return true;
}

function validarFrmRegUsu(FrmRegUsu){
	if(FrmRegUsu.TxtCct.value == ''){
            alert('Por favor capture su CLAVE DE CENTRO DE TRABAJO');
            FrmRegUsu.TxtCct.focus();
            return false;
	}
	if(FrmRegUsu.TxtNumeroAcuerdo.value == ''){
            alert('Por Favor capture su NÚMERO DE ACUERDO');
            FrmRegUsu.TxtNumeroAcuerdo.focus();
            return false;
	}
        if(FrmRegUsu.municipio.value == ''){
            alert('Por Favor seleccione un MUNICIPIO');
            FrmRegUsu.municipio.focus();
            return false;
	}
    
	if(FrmRegUsu.TxtCorreo.value == ''){
            alert('Por Favor capture una cuenta de CORREO ELECTRÓNICO válido\n ya que allí se le enviará una contraseña para acceder al sistema.');
            FrmRegUsu.TxtCorreo.focus();
            return false;
	}else{
            if(validarCorreo(FrmRegUsu.TxtCorreo.value))
                return true;
            else{
                alert('CORREO ELECTRÓNICO no válido');
                return false;
            }
    }
	
	
	
	
	
	return true;
}

function validarFrmEdtUsu(FrmEdtUsu){
    if(FrmEdtUsu.Passwd.value == ''){
            alert('NO se puede enviar el correo, NO se ha generado una NUEVA CONTRASEÑA.');
            FrmEdtUsu.TxtCorreo.focus();
            return false;
	}
	if(FrmEdtUsu.TxtCorreo.value == ''){
            alert('Por Favor capture una cuenta de CORREO ELECTRÓNICO válido\n ya que allí se le enviará una contraseña para acceder al sistema.');
            FrmEdtUsu.TxtCorreo.focus();
            return false;
	}else{
            if(validarCorreo(FrmEdtUsu.TxtCorreo.value)){
				
				
				//SI EL CORREO ALTERNATIVO  NO SE DEJA VACIO	
				if(FrmEdtUsu.TxtCorreoalternativo.value == ''){
					return true
				}else{

					if(validarCorreo(FrmEdtUsu.TxtCorreoalternativo.value)){
					 	return true;
					}else{
						alert('CORREO ALTERNATIVO no válido');
		                return false;		
					}	
				}
   	
				
			
			
				return true;
	
			}else{
                alert('CORREO ELECTRÓNICO no válido');
                return false;
            }
        }
	
	
	 
	return true;
}
function validarFrmImpVig(FrmImpVig){
     if(FrmImpVig.TxtCct.value == ''){
            alert('El campo CENTRO DE TRABAJO es requerido.');
            FrmImpVig.TxtCct.focus();
            return false;
	}
	if(FrmImpVig.TxtNumeroAcuerdo.value == ''){
            alert('El campo NÚMERO DE ACUERDO es requerido.');
            FrmImpVig.TxtNumeroAcuerdo.focus();
            return false;
	}
        if(FrmImpVig.Txtnivel.value == ''){
            alert('El campo Nivel Educativo es requerido');
            FrmImpVig.Txtnivel.focus();
            return false;
	}
        if(FrmImpVig.Txtnombre.value == ''){
            alert('El campo NOMBRE de Centro de Trabajo es requerido');
            FrmImpVig.Txtnombre.focus();
            return false;
	}
        if(FrmImpVig.Txtturno.value == ''){
            alert('El Campo TURNO es requerido.');
            FrmImpVig.Txtturno.focus();
            return false;
	}
        if(FrmImpVig.Txtpropietario.value == ''){
            alert('El campo PROPIETARIO es requerido.');
            FrmImpVig.Txtpropietario.focus();
            return false;
	}
        if(FrmImpVig.Txtdomicilio.value == ''){
            alert('El campo DOMICILIO es requerido.');
            FrmImpVig.Txtdomicilio.focus();
            return false;
	}
        if(FrmImpVig.municipio.value == ''){
            alert('El campo MUNICIPIO es requerido, por favor seleccione un municipio');
            FrmImpVig.municipio.focus();
            return false;
	}
       
        if(FrmImpVig.Txtfolio.value == ''){
            alert('El campo FOLIO es requerido.');
            FrmImpVig.Txtfolio.focus();
            return false;
	}
        return true;
 }

function validarCorreo(correo) {
    re=/^[_a-z0-9-]+(.[_a-z0-9-]+)*@[a-z0-9-]+(.[a-z0-9-]+)*(.[a-z]{2,3})$/
    if(!re.exec(correo))    {
        return false;
    }else{
        return true;
    }

 
}

