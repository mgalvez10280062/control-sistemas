<?php
       
    Include '../lib/_DbsIniPDO.php';
    $db_pdo = ConectaInformixUsuI7();
    

    
    $sql = "SELECT distinct(s.onomsis), s.kcvesis 
            FROM x1ctsis as s, i7emp_sis as r
            WHERE s.istatus != 'B'
              AND s.kcvesis = r.rcvesis";
    
    $rsltb = $db_pdo->query($sql);  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>controlsistemas</title>
        <link rel="stylesheet" href="css/escEstilos.css" media="screen" />   
                
        <script type="text/javascript">
           
            var colorOriginal;
                    
            function cambiarColor(action,renglon, var1){

                if(action==0){ 
                    renglon.style.backgroundColor = colorOriginal;
                    document.getElementById(var1).style.display = "none";
                }
                
                if(action==1){ 
                    colorOriginal = renglon.style.backgroundColor;
                    renglon.style.backgroundColor = '#CBECB5';

                    if(navigator.appName.indexOf("Microsoft") > -1){
                        document.getElementById(var1).style.display = "block";
                    }else{
                        document.getElementById(var1).style.display = "table-row";	
                    }		
                }
                
            }  
                    
                
            $(document).ready(function() {
                $("table.rayada tr:even").addClass("oddrow").delay(2000);
            });
            
        </script>
    </head>
    <body>     
        <?php Include 'inc/_Enc.php'; ?>
        <tr class="FondoConten">
        <td>
        <?php include("inc/_Enc6.php"); ?>
<!--        <form name="FrmDat" method="post" action="#">-->
            <input type="hidden" name="cct" id="cct" />
            <input type="hidden" name="acuerdo" id="acuerdo" />
                <br/>

                <table width="723" border="0" cellpadding="0" cellspacing="0" align="center">   
                    <tr>
                        <td valign="middle" class="tabboxColor">


                            <table width="950" class="tabInstrucciones">
                                <tr>    
                                    <td>
                                        <h2>Diccionario de Datos</h2>
                                    </td>   
                                </tr>       
                                <tr>
                                    <td>
                                        <h3>Elija tabla para consultar informaci&oacute;n de la misma</h3>
                                    </td>
                                </tr>
                            </table>

                            <table width="850" class="tabbox" align="center">
                                <tr>
                                    <td>                                   
                                        <table width="100%" border="0" class="rayada">                                              
                                            <tr style="background-color:#1b6d85; text-align:center; font:bold;">
                                                <td class="encAdminCCT" align="center">
                                                    No.
                                                </td>
                                                <td class="encAdminCCT" align="center" width="2%">
                                                    TABLA
                                                </td>
                                                <td class="encAdminCCT" align="center">   
                                                    TIPO DE DATO
                                                </td>
                                                <td class="encAdminCCT" align="center">
                                                    PK
                                                </td>
                                                <td class="encAdminCCT" align="center">
                                                    FK
                                                </td>

                                                <td class="encAdminCCT" align="center">
                                                    COMENTARIO
                                                </td>                                                
                                            </tr> 
                                            
                                            <?php  
                                                $cont=1;
                                                while($rowb = $rsltb->fetch(PDO::FETCH_ASSOC))
                                                { 
                                                    echo "<tr style='cursor:pointer;' onmouseover='cambiarColor(1,this, ".$cont.");' onmouseout='cambiarColor(0,this,".$cont.");' onclick=\"document.FrmDat.cct.value='" .$rowb["KCVESIS"] . "'; document.FrmDat.acuerdo.value='" . $rowb["KCVEEMP"] . "'; document.FrmDat.submit();\">";
                                                    echo "<td align=center height='20'>".$cont."</td>";

                                                    $sqle = "SELECT kcveemp, nom_emp, rcvesis 
                                                            FROM x1empleado_estable, i7emp_sis 
                                                            WHERE kcveemp = rcveemp 
                                                              AND rcvesis = ".$rowb["KCVESIS"];
                                                    $rslt = $db_pdo->query($sqle);

                                                    $cadena = '';
  
                                                    while($rowe = $rslt->fetch(PDO::FETCH_ASSOC))
                                                    {  
                                                        if($cadena == ''){
                                                            $cadena = $rowe["NOM_EMP"];
                                                        }else{
                                                            $cadena = $cadena .", ".$rowe["NOM_EMP"];
                                                        }    
                                                    }
                                                    
                                                    echo "<td algin=center height='20' width = '20%'>".$rowb["ONOMSIS"]."</td>";
                                                    echo "<td algin=center height='20'>".$cadena."</td>";
                                                    echo "<td height='20'><a href='i7diccionarioL.php'><center><input type='checkbox' name='PK' value='PK'></center></a></td>";
                                                    echo "<td height='20'><a href='i7cambioE.php'><center><input type='checkbox' name='FK' value='FK'></center></a></td>";
                                                    echo "<td height='20'><a href='#'><center></center></a></td>";                                               
                                                    echo "</tr>";
                                                    $cont++;    
                                                }   
                                            ?> 
                                        </table>                                   
                                    </td>
                                </tr>   
                            </table>           
                            <table align="center">
                                <tr>
                                    <td align="center">
                                        <br />
                                        <input type="button" class="tabBotLeft"  value="Regresar" 
                                        onclick="location.href='r3seleccionescuelaE.php';" />
                                    </td>
                                </tr>
                            </table>                     
                        </td>
                    </tr>
                </table>
                <br>
<!--            </form>-->
        </td>
        </tr>  
        <?php Include 'inc/_Pie.php'; ?>                      
    </body>
    
    
</html> 