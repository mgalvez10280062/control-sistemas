<?php
session_start();
require("../lib/_DbsIniPDO.php");

$Log = $_POST['TxtCuenta'];
$Psw = $_POST['TxtPass'];

$db_pdo = ConectaInformixUsuI7();

$sql = "SELECT otipusr, onomusu, ocuenta, odepto, ocargo
		FROM i7usuario
		WHERE ocuenta = :qcuenta
		AND opasswd = :qpassword
		AND ostatus = 'A'
		AND istatus in('A','M')";

$stmt = $db_pdo->prepare($sql);

$stmt->bindValue(':qcuenta', $Log, PDO::PARAM_STR);
$stmt->bindValue(':qpassword', $Psw, PDO::PARAM_STR);

$stmt->execute();
				
$row = $stmt->fetch(PDO::FETCH_ASSOC);				

if (!$row) {
		
		if (isset($_SESSION["SesUsrNombre"])) {
			session_name ("SesUsrNombre");
			session_unset();
			session_destroy();
		}
		
		if (isset($_SESSION["SesUsrTipo"])) {
			session_name ("SesUsrTipo");
			session_unset();
			session_destroy();
		}
		
		if (isset($_SESSION["SesUsrCuenta"])) {
			session_name ("SesUsrCuenta");
			session_unset();
			session_destroy();
		}
		
		if (isset($_SESSION["SesUsrDepto"])) {
			session_name ("SesUsrDepto");
			session_unset();
			session_destroy();
		}
		
		if (isset($_SESSION["SesUsrCargo"])) {
			session_name ("SesUsrCargo");
			session_unset();
			session_destroy();
		}
		
		mostrar_mensaje("<b>Registro Incorrecto.");
		
		
}
else{
	//echo 'entra';
	$_SESSION["SesUsrTipo"] = $row["OTIPUSR"];
	$_SESSION["SesUsrNombre"] = $row['ONOMUSU'];
	$_SESSION["SesUsrCuenta"] = $row['OCUENTA'];
	$_SESSION["SesUsrDepto"] = $row['ODEPTO'];
	$_SESSION["SesUsrCargo"] = $row['OCARGO'];

	header("location: i7sistemasL.php");
//	header("location: i6monitoreo.php");
}


function mostrar_mensaje($msj) {
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta name="GENERATOR" content="GURU"/>
	<title><? echo "Logeado a las " . date('Y/m/d H:i:s'); ?></title>
     <link rel="StyleSheet" href="css/estiloCongreso.css" type="text/css" />
</head>
<body leftmargin="0" topmargin="0">
    <br/><br/><br/><br/><br/>
    <center>
	<table border="0" cellpadding="0" cellspacing="0" class="tabbox">
            <tr>
                <td colspan="3" bgcolor="#CC4949" align="center"><font color="#FFFFFF" size="2" face="Arial,Helvetica">
				&nbsp;<b>Acceso denegado</b></font></td>
            </tr>
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>        
            <tr>
                <td align="center" colspan="3"><font size="2" face="Arial,Helvetica">
                       <center> <? echo $msj; ?></center>
					   <br /><br />			   
                    </font></td>
            </tr>    
            <tr>
                <td colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="3">
			    <input type="submit" value="Regresar" class="tabBotLeft" onClick="window.location='index.php'"/>
                </td>
            </tr>
        </table>
    </center>
    <br/><br/><br/><br/>
</body>
</html>
<?php }
  //  Include 'inc/_Pie.php';
	$db_pdo = NULL;
?>