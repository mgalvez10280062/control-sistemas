<?php
       
    Include '../lib/_DbsIniPDO.php';
    $db_pdo = ConectaInformixUsuI7();
    

    
    $sql = "SELECT distinct(s.onomsis), s.kcvesis 
            FROM x1ctsis as s, i7emp_sis as r
            WHERE s.istatus != 'B'
              AND s.kcvesis = r.rcvesis";
    
    $rsltb = $db_pdo->query($sql);  
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>controlsistemas</title>
        <link rel="stylesheet" href="css/escEstilos.css" media="screen" />   
                
        <script type="text/javascript">
           
            var colorOriginal;
                    
            function cambiarColor(action,renglon, var1){

                if(action==0){ 
                    renglon.style.backgroundColor = colorOriginal;
                    document.getElementById(var1).style.display = "none";
                }
                
                if(action==1){ 
                    colorOriginal = renglon.style.backgroundColor;
                    renglon.style.backgroundColor = '#CBECB5';

                    if(navigator.appName.indexOf("Microsoft") > -1){
                        document.getElementById(var1).style.display = "block";
                    }else{
                        document.getElementById(var1).style.display = "table-row";	
                    }		
                }
                
            }  
                    
                
            $(document).ready(function() {
                $("table.rayada tr:even").addClass("oddrow").delay(2000);
            });
            
        </script>
    </head>
    <body>     
        <?php Include 'inc/_Enc.php'; ?>
        <tr class="FondoConten">
        <td>
        <?php include("inc/_Enc6.php"); ?>
        <form name="FrmDat" method="post" >
            <input type="hidden" name="cct" id="cct" />
            <input type="hidden" name="acuerdo" id="acuerdo" />
                <br/>

                <table width="723" border="0" cellpadding="0" cellspacing="0" align="center">   
                    <tr>
                        <td valign="middle" class="tabboxColor">


                            <table width="950" class="tabInstrucciones">
                                <tr>    
                                    <td>
                                        <h2>Sistemas desarrollados</h2>
                                    </td>   
                                </tr>       
                                <tr>
                                    <td>
                                        <h3>Elija sistema para consultar informaci&oacute;n del mismo</h3>
                                    </td>
                                </tr>
                            </table>

                            <table width="850" class="tabbox" align="center">
                                <tr>
                                    <td>                                   
                                        <table width="100%" border="0" class="rayada">                                              
                                            <tr style="background-color:#1b6d85; text-align:center; font:bold;">
                                                <td class="encAdminCCT" align="center">
                                                    No.
                                                </td>
                                                <td class="encAdminCCT" align="center" width="2%">
                                                    SISTEMA
                                                </td>
                                                <td class="encAdminCCT" align="center">   
                                                    DESARROLLADOR
                                                </td>
                                                <td class="encAdminCCT" align="center">
                                                    DICCIONARIO DE DATOS
                                                </td>
                                                <td class="encAdminCCT" align="center">
                                                    CONTROL DE CAMBIO
                                                </td>

                                                <td class="encAdminCCT" align="center">
                                                    MANUAL TÉCNICO
                                                </td>
                                                <td class="encAdminCCT" align="center">
                                                    MANUAL DE USUARIO
                                                </td>
                                            </tr> 
                                            
                                            <?php  
                                                $cont=1;
                                                while($rowb = $rsltb->fetch(PDO::FETCH_ASSOC))
                                                { 
                                                    echo "<tr style='cursor:pointer;' onmouseover='cambiarColor(1,this, ".$cont.");' onmouseout='cambiarColor(0,this,".$cont.");' onclick=\"document.FrmDat.cct.value='" .$rowb["KCVESIS"] . "'; document.FrmDat.acuerdo.value='" . $rowb["KCVEEMP"] . "'; document.FrmDat.submit();\">";
                                                    echo "<td align=center height='20'>".$cont."</td>";

                                                    $sqle = "SELECT kcveemp, nom_emp, rcvesis 
                                                            FROM x1empleado_estable, i7emp_sis 
                                                            WHERE kcveemp = rcveemp 
                                                              AND rcvesis = ".$rowb["KCVESIS"];
                                                    $rslt = $db_pdo->query($sqle);

                                                    $cadena = '';
  
                                                    while($rowe = $rslt->fetch(PDO::FETCH_ASSOC))
                                                    {  
                                                        if($cadena == ''){
                                                            $cadena = $rowe["NOM_EMP"];
                                                            $vcvesis = $rowe["RCVESIS"];
                                                        }else{
                                                            $cadena = $cadena .", ".$rowe["NOM_EMP"];
                                                            $vcvesis = $rowe["RCVESIS"];
                                                        }    
                                                    }
                                                    
//                                                    echo "<td algin=center height='20' width = '20%'><a href='i7seccion1L.php'>".$rowb["ONOMSIS"]."</a></td>";
                                                    echo "<td algin=center height='20' width = '20%'>".$rowb["ONOMSIS"]."</td>";
                                                    echo "<td algin=center height='20'>".$cadena."</td>";
                                                    echo "<td height='20'><a href='i7diccionarioL.php'><center><img src='img/dicdat.png' width='25' height='25'></center></a></td>";
                                                    echo "<td height='20'><a href='i7seccion1L.php?a=".$vcvesis."'><center><img src='img/cambios.png' width='25' height='25'></center></a></td>";
                                                    echo "<td height='20'><a href='i7manualtecL.php?a=".$vcvesis."' title='Manual t&eacute;cnico'><center><img src='img/manual.png' width='25' height='25'></center></a></td>";
                                                    echo "<td height='20'><a href='i7manualusuL.php' title='Manual de usuario'><center><img src='img/manual.png' width='25' height='25'></center></a></td>";
//                                                    echo "<td heiht='20'><a href='#'><center><img src='img/manual.png' width='25' height='25'></center></a></td>";

                                                    echo "</tr>";
                                                    $cont++;    
                                                }   
                                            ?> 
                                        </table>                                   
                                    </td>
                                </tr>   
                            </table> 
                            <br/>
                            <br/>
                        </td>
                    </tr>
                </table>
                <br>
            </form>
        </td>
        </tr>  
        <?php Include 'inc/_Pie.php'; ?>                      
    </body>
    
    
</html> 